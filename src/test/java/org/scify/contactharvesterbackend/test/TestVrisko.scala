package org.scify.contactharvesterbackend.test

import org.junit.Test
import org.scify.contactharvesterbackend.inteligence.{JsonTool, RandomTool}
import java.util.{ArrayList, List}

import org.scify.contactharvesterbackend.inteligence.contactExtraction.{ExtractVriskoPagesUsingKeywords, VriskoSearch}

import collection.mutable.{HashMap, HashSet}


/**
  * Created by akosmo on 5/5/2016.
  */
class TestVrisko {

  def test {
    val keywords: List[String] = new ArrayList[String]
    keywords.add("PC")
    keywords.add("πλαστικοποίηση")
    val ex: ExtractVriskoPagesUsingKeywords = new ExtractVriskoPagesUsingKeywords(keywords)
    val extractedUrls: HashMap[String, HashSet[String]] = ex.extractUrls
    val vs = new VriskoSearch
    extractedUrls.foreach(e => {
      val query = e._1
      e._2.foreach(link => {
        vs.extractForUrl(link,query)
        val sleepValue = 1000 + (RandomTool.generateProb() * 1000).toInt
        Thread.sleep(sleepValue)
      })
    })
    vs.saveJson("vrisko.json")
    val results = JsonTool.readVriskoJson("vrisko.json")
    results.foreach(e => println(e.name))
  }


  def testTelephone(): Unit = {
    val vs = new VriskoSearch
    vs.extractForUrl("http://www.vrisko.gr/search/%CE%BA%CE%AC%CF%81%CF%84%CE%B5%CF%82/","")
    vs.contacts.foreach(e => println(e))
  }

}
