package org.scify.contactharvesterbackend.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by akosmo on 25/4/2016.
 */
@Document(collection = "searches")
public class ExtractedContactsClassificationSequence {
    @Id
    private String id;

    @Indexed
    private int userId;

    @Indexed
    private int searchId;

    private List<ClassificationInfoPerInstance> sequence;

    private List<Integer> positive;

    private List<Integer> negative;

    public ExtractedContactsClassificationSequence() {

    }

    public ExtractedContactsClassificationSequence(int userId, int searchId,List<Integer> positive, List<Integer> negative) {
        this.userId = userId;
        this.searchId = searchId;
        this.positive = positive;
        this.negative = negative;
        id = "ClassifiedContacts" + userId + ":" + searchId;
        sequence = new ArrayList<>();
    }

    public void setId (int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setSearchId (int searchId) {
        this.searchId = searchId;
    }

    public int getSearchId () {
        return searchId;
    }

    public void setSequence(List<ClassificationInfoPerInstance> sequence) {
        this.sequence = sequence;
    }

    public List<ClassificationInfoPerInstance> getSequence () {
        return sequence;
    }

    public void setPositive (List<Integer> positive) {
        this.positive = positive;
    }

    public List<Integer> getPositive () {
        return positive;
    }

    public void setNegative (List<Integer> negative) {
        this.negative = negative;
    }

    public List<Integer> getNegative () {
        return negative;
    }
}
