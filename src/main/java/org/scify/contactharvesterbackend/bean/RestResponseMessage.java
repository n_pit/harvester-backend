package org.scify.contactharvesterbackend.bean;

/**
 * Created by akosmo on 18/4/2016.
 */
public class RestResponseMessage<T> {
    private String status, details, type;
    private T data;

    public RestResponseMessage (String status, String details, String type,T data) {
        this.status = status;
        this.details = details;
        this.type = type;
        this.data = data;
    }

    public RestResponseMessage (String status, String details, String type) {
        this.status = status;
        this.details = details;
        this.type = type;
        data = null;
    }

    public String getStatus () {
        return status;
    }

    public void setStatus (String status) {
        this.status = status;
    }

    public String getDetails () {
        return details;
    }

    public void setDetails (String details) {
        this.details = details;
    }

    public String getType() {
        return type;
    }

    public void setType (String type) {
        this.type = type;
    }

    public T getData () {
        return data;
    }

    public void setData (T data) {
        this.data = data;
    }
}

