package org.scify.contactharvesterbackend.bean;

/**
 * Created by akosmo on 3/6/2016.
 */
public class ClassificationInfoPerInstance {
    private int contactId;
    private Double probability;
    private String annotated;

    public ClassificationInfoPerInstance () {
        contactId = 0;
        probability = 0.0;
        annotated = "unannotated";
    }

    public ClassificationInfoPerInstance(int id, double probability, String annotated) {
        contactId = id;
        this.probability = probability;
        this.annotated = annotated;
    }

    public void setContactId (int id) {
        contactId = id;
    }

    public int getContactId () {
        return contactId;
    }

    public void setProbability (double probability) {
        this.probability = probability;
    }

    public Double getProbability () {
        return probability;
    }

    public void setAnnotated (String annotated) {
        this.annotated = annotated;
    }

    public String getAnnotated () {
        return annotated;
    }

}
