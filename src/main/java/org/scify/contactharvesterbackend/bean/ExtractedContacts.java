package org.scify.contactharvesterbackend.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by akosmo on 25/4/2016.
 */
@Document(collection = "searches")
public class ExtractedContacts {
    @Id
    private String id;

    @Indexed
    private int userId;

    @Indexed
    private int searchId;

    private List<String> keywords;

    private int topResultsPerSearch;

    private String extraStringPerSearch;

    private List<ExtractedContact> contacts;

    public ExtractedContacts() {

    }

    public ExtractedContacts(int userId, int searchId, List<String> keywords, int topResultsPerSearch, String extraStringPerSearch) {
        this.userId = userId;
        this.searchId = searchId;
        this.topResultsPerSearch = topResultsPerSearch;
        this.extraStringPerSearch = extraStringPerSearch;
        id = "ExtractedContacts" + userId + ":" + searchId;
        this.keywords = keywords;
        contacts = new ArrayList<>();
    }

    public void setId (int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setSearchId (int searchId) {
        this.searchId = searchId;
    }

    public int getSearchId () {
        return searchId;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public List<String> getKeywords () {
        return keywords;
    }

    public void setTopResultsPerSearch (int topResultsPerSearch) {
        this.topResultsPerSearch = topResultsPerSearch;
    }

    public int getTopResultsPerSearch() {
        return topResultsPerSearch;
    }

    public void setExtraStringPerSearch(String extraStringPerSearch) {
        this.extraStringPerSearch = extraStringPerSearch;
    }

    public String getExtraStringPerSearch() {
        return extraStringPerSearch;
    }

    public void setContacts (List<ExtractedContact> contacts) {
        this.contacts = contacts;
    }

    public List<ExtractedContact> getContacts () {
        return contacts;
    }
}
