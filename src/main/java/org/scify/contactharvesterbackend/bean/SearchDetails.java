package org.scify.contactharvesterbackend.bean;

import org.scify.contactharvesterbackend.enumerations.SearchStatus;
import org.scify.contactharvesterbackend.enumerations.SearchStep;

/**
 * Created by akosmo on 19/4/2016.
 */
public class SearchDetails {
    //created keywords contacts
    private SearchStep step = SearchStep.SearchingForContacts;
    //running canceled completed
    private SearchStatus status = SearchStatus.Unknown;

    private int progressBar = 0;

    public SearchStep getStep () {
        return step;
    }

    public void setStep (SearchStep step) {
        this.step = step;
    }


    public SearchStatus getStatus () {
        return status;
    }

    public void setStatus (SearchStatus status) {
        this.status = status;
    }

    public int getProgressBar () {
        return progressBar;
    }

    public void setProgressBar (int progressBar) {
        this.progressBar = progressBar;
    }
}
