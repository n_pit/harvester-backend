package org.scify.contactharvesterbackend.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by akosmo on 22/4/2016.
 */
@Document(collection = "searches")
public class ExtractedKeywords {
    @Id
    private String id;

    @Indexed
    private int userId;

    @Indexed
    private int searchId;

    private List<String> urls;

    private List<String> keywords;

    public ExtractedKeywords() {

    }

    public ExtractedKeywords(int userId, int searchId, List<String> urls) {
        this.userId = userId;
        this.searchId = searchId;
        id = "ExtractedKeywords" + userId + ":" + searchId;
        this.urls = urls;
        keywords = new ArrayList<>();
    }

    public int getUserId () {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getSearchId() {
        return searchId;
    }

    public void setSearchId(int searchId) {
        this.searchId = searchId;
    }

    public List<String> getUrls () {
        return urls;
    }

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    public List<String> getKeywords () {
        return keywords;
    }

    public void setKeywords (List<String> keywords) {
        this.keywords = keywords;
    }


}
