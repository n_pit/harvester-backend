package org.scify.contactharvesterbackend.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;

/**
 * Created by akosmo on 19/4/2016.
 */
@Document(collection = "searches")
public class SearchKeeper {
    @Id
    private String id="SearchKeeperId";

    //user id to map of search id to search
    private HashMap<Integer,HashMap<Integer,SearchDetails> > userIdToSearches = new HashMap<>();

    public HashMap<Integer,HashMap<Integer,SearchDetails> > getUserIdToSearches () {
        return userIdToSearches;
    }

    public void setUserIdToSearches (HashMap<Integer,HashMap<Integer,SearchDetails> > userIdToSearches) {
        this.userIdToSearches = userIdToSearches;
    }

    public SearchDetails getSearchOfUser (int userId,int searchId) {
        if (userIdToSearches.containsKey(userId)) {
            HashMap<Integer,SearchDetails> searches = userIdToSearches.get(userId);
            if (searches.containsKey(searchId)) {
                return searches.get(searchId);
            }else {
                SearchDetails ret = new SearchDetails();
                searches.put(searchId,ret);
                return ret;
            }
        }else {
            HashMap<Integer,SearchDetails> searches = new HashMap<>();
            SearchDetails ret = new SearchDetails();
            searches.put(searchId,ret);
            userIdToSearches.put(userId,searches);
            return ret;
        }
    }

}
