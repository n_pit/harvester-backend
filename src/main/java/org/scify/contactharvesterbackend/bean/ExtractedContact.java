package org.scify.contactharvesterbackend.bean;
/**
 * Created by akosmo on 15/4/2016.
 */
public class ExtractedContact {
    protected String domain;
    protected String email;
    protected String phone;
    protected String address;
    protected String facebookURL;
    protected String twitterURL;
    protected String additionalInfo;
    protected String relatedQueries;
    protected int contactId;

    public ExtractedContact(){}

    public ExtractedContact(String domain, String email, String phone, String address, String facebook, String twitter, String additionalInfo, String relatedQueries, int contactId) {
        this.domain = domain;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.facebookURL = facebook;
        this.twitterURL = twitter;
        this.additionalInfo = additionalInfo;
        this.relatedQueries = relatedQueries;
        this.contactId = contactId;
    }

    public ExtractedContact(String domain, String email, String phone, String address, String facebook, String twitter, String additionalInfo, String relatedQueries) {
        this.domain = domain;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.facebookURL = facebook;
        this.twitterURL = twitter;
        this.additionalInfo = additionalInfo;
        this.relatedQueries = relatedQueries;
        this.contactId = 0;
    }

    public int getContactId() {
        return contactId;
    }

    public void setContactId (int contactId) {
        this.contactId = contactId;
    }

    public String getDomain() {
        return domain.trim();
    }

    public void setDomain (String domain) {
        this.domain = domain.trim();
    }

    public String getEmail () {
        return email;
    }

    public void setEmail (String email) {
        this.email = email;
    }

    public String getPhone () {
        return phone;
    }

    public void setPhone (String phone) {
        this.phone = phone;
    }

    public String getAddress () {
        return address;
    }

    public void setAddress (String address) {
        this.address = address;
    }

    public String getFacebookURL() {
        return facebookURL;
    }


    public void setFacebookURL(String facebook) {
        this.facebookURL = facebook;
    }

    public String getTwitterURL() {
        return twitterURL;
    }

    public void setTwitterURL(String twitter) {
        this.twitterURL = twitter;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getRelatedQueries() {
        return relatedQueries;
    }

    public void setRelatedQueries(String relatedQueries) {
        this.relatedQueries = relatedQueries;
    }

}
