package org.scify.contactharvesterbackend.enumerations;

/**
 * Created by akosmo on 19/4/2016.
 */
public enum SearchStep {
    SearchingForKeywords, SearchingForContacts, ClassifyingContacts
}
