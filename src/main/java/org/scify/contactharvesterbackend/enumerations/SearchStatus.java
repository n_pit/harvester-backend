package org.scify.contactharvesterbackend.enumerations;

/**
 * Created by akosmo on 19/4/2016.
 */
public enum SearchStatus {
    running,canceled,completed,failed,pending,Unknown
}
