package org.scify.contactharvesterbackend.inteligence

import scala.util.Random

/**
  * Created by akosmo on 5/5/2016.
  */
object RandomTool {
  private val random = new Random()

  def generateBetween(min:Int,max:Int):Int = {
    random.nextInt(max-min+1) + min
  }

  def generateProb ():Double = random.nextDouble()
}
