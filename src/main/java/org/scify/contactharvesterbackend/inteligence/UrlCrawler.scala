package org.scify.contactharvesterbackend.inteligence

import java.io.{BufferedReader, File, FileReader, FileWriter}
import java.net.{URL, URLConnection}

import net.htmlparser.jericho.{Source, _}
import org.apache.logging.log4j.LogManager
import org.scify.contactharvesterbackend.inteligence.contactExtraction.Contacts

import scala.collection.JavaConversions._
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}


/**
  * Created by akosmo on 29/1/2016.
  */
class UrlCrawler() {

  private val logger = LogManager.getRootLogger

  /**
    * Get all text from a specific url
    *
    * @param urlString
    * @return
    */
  def getAllText(urlString: String): String = {
    val url = new URL(urlString)
    val con = url.openConnection()
    con.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0")
    val html = new Source(con)
    val basicPage = html.getAllElements("table").iterator().next()
    basicPage.getTextExtractor.toString
  }

  /**
    * Get all text from a list of specific urls as one String
    *
    * @param urlStrings
    * @return
    */
  def getAllText(urlStrings: java.util.List[String]): String = {
    var ret = ""
    urlStrings.foreach(urlString => {
      try {
        val url = new URL(urlString)
        val con = url.openConnection()
        con.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0")
        val html = new Source(con)
        ret = ret + html.getTextExtractor.toString + " "
      }
      catch {
        case e:Exception => logger.info("(UrlCrawler) url: " + urlString + " failed with exception: (" + e +")")
      }
    })
    ret
  }

  /**
    * Get all urls from a specific url
    *
    * @param urlString
    * @return
    */
  def getAllLinks(urlString: String): java.util.List[String] = {
    var ret = new java.util.ArrayList[String]()
    val url = new URL(urlString)
    val con = url.openConnection()
    con.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0")
    val html = new Source(con)
    val links = html.getAllElements("a").toArray().map(e => e.asInstanceOf[Element])
    links.foreach(link => {
      val iter = link.getAttributes.iterator()
      while (iter.hasNext) {
        val att = iter.next()
        if (att.getKey == "href")
          ret += att.getValue
      }
    })
    ret
  }

  /**
    * Get all urls from a list of specific urls
    *
    * @param urlStrings
    * @return
    */
  def getAllLinks(urlStrings: java.util.ArrayList[String]): java.util.ArrayList[String] = {
    var ret = new java.util.ArrayList[String]()
    urlStrings.foreach(urlString => {
      val url = new URL(urlString)
      val con = url.openConnection()
      con.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0")
      val html = new Source(con)
      val links = html.getAllElements("a").toArray().map(e => e.asInstanceOf[Element])
      links.foreach(link => {
        val iter = link.getAttributes.iterator()
        while (iter.hasNext) {
          val att = iter.next()
          if (att.getKey == "href")
            ret += att.getValue
        }
      })
    })
    ret
  }


  def crawlPagesToFile(fileWithUrls: String, outputDirectory: String): Unit = {
    crawlPagesToFile(fileWithUrls, outputDirectory, 0)
  }

  def crawlPagesToFile(fileWithUrls: String, outputDirectory: String, startingIndex: Int): Unit = {
    val file = new File(fileWithUrls)
    if (file.exists()) {
      val fr = new FileReader(file)
      val br = new BufferedReader(fr)
      var line = br.readLine()
      val links = new ArrayBuffer[String]
      while (line != null) {
        links += line
        line = br.readLine()
      }
      br.close()
      fr.close()
      crawlPagesToFile(links, outputDirectory, startingIndex)
    } else {
      logger.info("(UrlCrawler) File " + fileWithUrls + " does not exist")
    }
  }

  private def crawlPagesToFile(urls: ArrayBuffer[String], outputDirectory: String, startingIndex: Int): Unit = {
    for (i <- startingIndex until urls.size) {
      val link = urls(i)
      def operation(con: URLConnection): Boolean = {
        val html = new Source(con)
        val outputDocument = new OutputDocument(html)
        val wr = new FileWriter(new File(outputDirectory + "/" + i))
        outputDocument.writeTo(wr)
        logger.info("(UrlCrawler) Link " + link + "(" + i + ") was downloaded successfully, " + (urls.size - i) + " links left.")
        true
      }
      try {
        val url = new URL(link)
        val con = url.openConnection()
        con.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0")
        implicit val ec = ExecutionContext.global
        val f = Future {
          operation(con)
        }
        Await.result(f, Duration.apply("10 seconds"))
      } catch {
        case e: Exception => logger.info("(UrlCrawler) Problem with url " + link + " with exception + " + e)
      }
    }
  }

  def extractContactInfoFromFile (fileName:String,url:String): Contacts = {
    val ret = new Contacts(url)
    try {
      val file = new File(fileName)
      val html = new Source(file)
      val linkElements = html.getAllElements("a").toArray().map(e=>e.asInstanceOf[Element])
      val links = new ArrayBuffer[String]
      linkElements.foreach(link => {
        val iter = link.getAttributes.iterator()
        while (iter.hasNext) {
          val att = iter.next()
          if (att.getKey == "href")
            links += att.getValue
        }
      })
      val facebookLinks = links.filter(e => e.contains("www.facebook")).filter(e=>e.substring(0,2)!="//").filter(e=> e.contains(" ") == false).filter(e => e.contains("?") == false).filter(e=>e.length < 100)
      facebookLinks.foreach(e => ret.facebook += e)
      val twitterLinks = links.filter(e=> e.contains("twitter.com")).filter(e=>e.substring(0,2)!="//").filter(e=> e.contains(" ") == false).filter(e => e.contains("?") == false)
      twitterLinks.foreach(e => ret.twitter += e)
      val linkedIn = links.filter(e=> e.contains("www.linkedin")).filter(e=>e.substring(0,2)!="//").filter(e=> e.contains(" ") == false).filter(e => e.contains("?") == false)
      linkedIn.foreach(e => ret.linkedIn += e)
    }catch {
      case e:Exception =>logger.info("(UrlCrawler) Problem with url " + url + " with exception + " + e)
        return null
    }
    ret
  }

}
