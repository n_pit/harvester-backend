package org.scify.contactharvesterbackend.inteligence

import akka.actor.{Actor, ActorLogging}
import org.scify.contactharvesterbackend.basic.SetMongoTemplate
import org.scify.contactharvesterbackend.bean.SearchKeeper
import org.scify.contactharvesterbackend.enumerations.{SearchStatus, SearchStep}
import org.scify.contactharvesterbackend.inteligence.classification.StartContactClassificationMessage
import org.scify.contactharvesterbackend.inteligence.contactExtraction.StartContactsSearchMessage
import org.scify.contactharvesterbackend.inteligence.keywordsExtraction.StartKeywordsSearchMessage
import org.springframework.data.mongodb.core.MongoTemplate

/**
  * Created by akosmo on 22/4/2016.
  */
class SearchKeeperActor () extends Actor with ActorLogging {

  private var mongoTemplate:MongoTemplate = null
  private var sk:SearchKeeper = null
  private var skNeedsLoading = true
  private def syncSearchKeeper(): Unit = {
    if (skNeedsLoading) {
      sk = mongoTemplate.findById("SearchKeeperId",classOf[SearchKeeper],"searches")
      skNeedsLoading = false
    }
  }

  def receive = {
    case SetMongoTemplate(mongoTemplate) => {
      this.mongoTemplate = mongoTemplate
    }

    case StartKeywordsSearchMessage(userId,searchId,urls) => {
      syncSearchKeeper()
      val searchDetails = sk.getSearchOfUser(userId,searchId)
      searchDetails.setProgressBar(0)
      searchDetails.setStatus(SearchStatus.pending)
      searchDetails.setStep(SearchStep.SearchingForKeywords)
      mongoTemplate.save(sk,"searches")
    }

    case StartContactsSearchMessage(userId,searchId,keywords,maxTopResultsPerSearch,extraStringPerSearch) => {
      syncSearchKeeper()
      val searchDetails = sk.getSearchOfUser(userId,searchId)
      searchDetails.setProgressBar(0)
      searchDetails.setStatus(SearchStatus.pending)
      searchDetails.setStep(SearchStep.SearchingForContacts)
      mongoTemplate.save(sk,"searches")
    }

    case StartContactClassificationMessage(userId,searchId,positive,negative) => {
      syncSearchKeeper()
      val searchDetails = sk.getSearchOfUser(userId,searchId)
      searchDetails.setProgressBar(0)
      searchDetails.setStatus(SearchStatus.pending)
      searchDetails.setStep(SearchStep.ClassifyingContacts)
      mongoTemplate.save(sk,"searches")
    }

    case AlterSearchDetails(userId,searchId,progressBar,status,step) => {
      syncSearchKeeper()
      val searchDetails = sk.getSearchOfUser(userId,searchId)
      if (progressBar != -1)
        searchDetails.setProgressBar(progressBar)
      if (status != null)
        searchDetails.setStatus(status)
      if (step != null)
        searchDetails.setStep(step)
      mongoTemplate.save(sk,"searches")
    }

  }
}

case class AlterSearchDetails(userId:Int,searchId:Int,progressBar:Int,status:SearchStatus,step:SearchStep)

