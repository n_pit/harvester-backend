package org.scify.contactharvesterbackend.inteligence.keywordsExtraction

import java.util

import scala.collection.mutable.{ArrayBuffer, HashMap}

/**
  * Created by akosmo on 29/1/2016.
  */
class TermExtractor {

  val bestTerms = new ArrayBuffer[String]()

  def extractBestTerms (text:String, minimumSize:Int, maxNumberOfTerms:Int): Unit = {
    val initialTerms = text.split(" ")
    val bigEnoughTerms = initialTerms.filter(e => e.length >= minimumSize)
    val frequency = new HashMap[String,Int]()
    bigEnoughTerms.foreach(e => {
      val term = e.replaceAll("[`~!@#$%^&*'\";:/?>.<,\\|()]", "").toLowerCase()
      frequency.get(term) match {
        case Some(x) => frequency += term -> (x+1)
        case None => frequency +=  term -> 1
      }
    })
    val sorted = frequency.toArray.sortWith((a,b)=> a._2 > b._2)
    val finalBestTermsSize = if (sorted.size > maxNumberOfTerms) maxNumberOfTerms else sorted.size
    for (i <- 0 until finalBestTermsSize)
      bestTerms += sorted(i)._1
  }

  def getAllBestTerms ():util.ArrayList[String] = {
    val ret = new util.ArrayList[String]()
    bestTerms.foreach(term => ret.add(term))
    ret
  }

  def printBest (): Unit = {
    bestTerms.foreach(println)
  }

  def extractBestTerms(terms:String):Unit ={
    bestTerms.clear()
    terms.split(",").foreach(e => {
      bestTerms += e.trim
    })
  }

}
