package org.scify.contactharvesterbackend.inteligence.keywordsExtraction

import java.util

import akka.actor.{Actor, ActorLogging, ActorRef, PoisonPill}
import org.apache.logging.log4j.LogManager
import org.scify.contactharvesterbackend.basic.{CancelOperationMessage, Settings, StartOperationMessage}
import org.scify.contactharvesterbackend.bean.ExtractedKeywords
import org.scify.contactharvesterbackend.enumerations.SearchStatus
import org.scify.contactharvesterbackend.inteligence.{AlterSearchDetails, UrlCrawler}
import org.springframework.data.mongodb.core.MongoTemplate

/**
  * Created by akosmo on 22/4/2016.
  */
class KeywordExtractionOperation(searchKeeperActor: ActorRef, keywordsExtractorActor:ActorRef,userId: Int, searchId: Int, urls: java.util.List[String], mongoTemplate: MongoTemplate) extends Actor with ActorLogging {
  private val exKeywords = new ExtractedKeywords(userId, searchId, urls)
  private var canceled = false

  private var crawledPages = ""


  def receive: Receive = {
    case StartOperationMessage => initiateExtraction()
    case KeExOpCrawlPagesMessage => crawlPages()
    case KeExOpExtractKeywordsMessage => extractKeywords()
    case CancelOperationMessage => cancel()
    case _ => LogManager.getRootLogger.info("Wrong message at KeywordExtractionOperation")
  }

  private def initiateExtraction(): Unit = {
    mongoTemplate.save(exKeywords)
    searchKeeperActor ! AlterSearchDetails(userId,searchId,10,SearchStatus.running,null)
    self ! KeExOpCrawlPagesMessage
  }

  private def crawlPages (): Unit = {
    val cr = new UrlCrawler
    crawledPages = cr.getAllText(urls)
    searchKeeperActor ! AlterSearchDetails(userId,searchId,50,null,null)
    self !KeExOpExtractKeywordsMessage
  }

  private def extractKeywords (): Unit = {
    val termExtractor = new TermExtractor
    termExtractor.extractBestTerms(crawledPages,Settings.minKeywordLength,Settings.maxKeywordsCrawled)
    val keywords = new util.ArrayList[String]()
    termExtractor.bestTerms.foreach(e=> keywords.add(e))
    exKeywords.setKeywords(keywords)
    mongoTemplate.save(exKeywords)
    searchKeeperActor ! AlterSearchDetails(userId,searchId,100,SearchStatus.completed,null)
    keywordsExtractorActor ! KeywordExtractionOperationTerminatedMessage(userId + ":" + searchId)
    self ! PoisonPill
  }

  private def cancel (): Unit = {
    if (canceled == false) {
      canceled = true
      searchKeeperActor ! AlterSearchDetails(userId, searchId, 50, SearchStatus.canceled, null)
      keywordsExtractorActor ! KeywordExtractionOperationTerminatedMessage(userId + ":" + searchId)
      self ! PoisonPill
    }
  }

  case object KeExOpCrawlPagesMessage
  case object KeExOpExtractKeywordsMessage

}

