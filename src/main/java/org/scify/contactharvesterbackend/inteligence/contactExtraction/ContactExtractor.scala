package org.scify.contactharvesterbackend.inteligence.contactExtraction

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import org.apache.logging.log4j.LogManager
import org.scify.contactharvesterbackend.basic.{CancelOperationMessage, Settings, StartOperationMessage}
import org.springframework.data.mongodb.core.MongoTemplate

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  * Created by akosmo on 22/4/2016.
  */
class ContactExtractor (searchKeeperActor: ActorRef,actorSystem:ActorSystem, mongoTemplate: MongoTemplate) extends Actor with ActorLogging {

  private val actorOperationsMap = new mutable.HashMap[String,ActorRef]()
  private var pendingOperations = new ListBuffer[ActorRef]
  private val max = Settings.maxContactSearches
  private var activeSearches = 0

  def receive: Receive = {
    case StartContactsSearchMessage(userId,searchId,keywords,maxTopResultsPerSearch,extraStringPerSearch) => {
      val key = userId + ":" + searchId
      val props: Props = Props(new ContactExtractionOperation(searchKeeperActor,self,userId,searchId,keywords,maxTopResultsPerSearch,extraStringPerSearch,mongoTemplate))
      val actor = actorSystem.actorOf(props)

      actorOperationsMap += key -> actor
      if (activeSearches < max) {
        actor ! StartOperationMessage
        activeSearches += 1
      }else {
          pendingOperations += actor
      }
    }

    case CancelContactsSearchMessage(userId,searchId) => {
      val key = userId + ":" + searchId
      actorOperationsMap.get(key) match {
        case Some(x) => x ! CancelOperationMessage
        case None => LogManager.getRootLogger.info("No search contacts operation with userId:" + userId + " and searchId:" + searchId)
      }
    }
    case ContactExtractionOperationTerminatedMessage(key) => {
      actorOperationsMap.remove(key)
      if (pendingOperations.size == 0)
        activeSearches -= 1
      else {
        val actor = pendingOperations.head
        pendingOperations = pendingOperations.tail
        actor ! StartOperationMessage
      }
    }
  }
}
case class StartContactsSearchMessage(userId: Int, searchId: Int, keywords: java.util.List[String] = null, maxTopResultsPerSearch:Int = 10, extraStringPerSearch:String = "")
case class CancelContactsSearchMessage(userId: Int, searchId: Int)
case class ContactExtractionOperationTerminatedMessage(key:String)