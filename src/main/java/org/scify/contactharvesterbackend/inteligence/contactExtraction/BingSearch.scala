package org.scify.contactharvesterbackend.inteligence.contactExtraction

import java.io.{BufferedReader, InputStreamReader}
import java.net.{URL, URLEncoder}

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64

/**
  * Created by akosmo on 29/1/2016.
  */
class BingSearch(key:String) {

  def search (query:String):String = {
    val urlString = "https://api.datamarket.azure.com/Bing/Search/Web?$format=json&Query=" +
      URLEncoder.encode("'" + query + "'", "UTF-8");
    val url = new URL(urlString)
    val con = url.openConnection()
    val sUserPass = key + ":" + key
    val encoding = Base64.encode(sUserPass.getBytes());
    con.setRequestProperty("Authorization", "Basic " + encoding)
    con.connect()
    val html = new BufferedReader(new InputStreamReader(con.getInputStream) )
    val ret = html.readLine()
    html.close()
    ret

  }

}
