package org.scify.contactharvesterbackend.inteligence.contactExtraction

import java.io.{BufferedWriter, File, FileWriter}
import java.net.URL

import net.htmlparser.jericho.Source
import net.liftweb.json.JsonDSL._
import net.liftweb.json._
import org.apache.logging.log4j.LogManager

import scala.collection.mutable

/**
  * Created by akosmo on 24/3/2016.
  */
class VriskoSearch {

  val contacts = new mutable.HashMap[String,VriskoContact]()

  private val logger = LogManager.getRootLogger

  def extractForUrl(urlString:String, query:String): Unit = {
    var step = 0
    var advStep = 0
    var freeStep = 0
    var subStep = 0
    try {
      val url = new URL(urlString)
      val con = url.openConnection()
      con.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0")
      val html = new Source(con)
      val resultsPart =  html.getElementById("SearchResults")
      step += 1
      val advBoxes = resultsPart.getAllElementsByClass("AdvItemBox")
      step += 1
      val freeBoxes = resultsPart.getAllElementsByClass("FreeListingItemBox")
      step += 1
      if (advBoxes.size() > 0) {
        val iter = advBoxes.iterator()
        while (iter.hasNext) {
          advStep += 1
          subStep = 0
          val box = iter.next()
          subStep += 1
          val companyName = box.getAllElementsByClass("CompanyName").iterator().next().getTextExtractor.toString
          subStep += 1
          val description = box.getAllElementsByClass("CompanyDescr").iterator().next().getTextExtractor.toString
          subStep += 1
          val websites = box.getAllElementsByClass("AdvSiteWebSite")
          subStep += 1
          var webSite = if (websites.size() == 0) "" else websites.iterator().next().getAttributes.get("href").getValue
          subStep += 1
          if (webSite.trim == "http://") webSite = ""
          subStep += 1
          val address = box.getAllElementsByClass("AdvAddress").iterator().next().getTextExtractor.toString
          subStep += 1
          val categoryBox = box.getAllElementsByClass("AdvCategoryArea")
          val category = if (categoryBox.size() > 0 ) categoryBox.iterator().next().getTextExtractor.toString else ""
          subStep += 1


          val telephones = new mutable.HashSet[String]
          val tIter = box.getAllElements("itemprop","telephone",true).iterator()
          while (tIter.hasNext) {
            telephones += tIter.next().getTextExtractor.toString
          }
          var telephone = ""
          telephones.foreach(e=> telephone += e + " ")
          val emails = new mutable.HashSet[String]()
          val eIter = box.getAllElements("itemprop","email",true).iterator()
          while (eIter.hasNext) {
            val element = eIter.next()
            val hrefPart = element.getAttributeValue("href")
            if (hrefPart != null) {
              if (hrefPart.contains(":") && hrefPart.contains("www.vrisko.gr/email/") == false)
                emails += hrefPart.split(":")(1).trim
            }
          }
          var email = ""
          emails.foreach(e => email +=  e + " ")
          contacts += companyName -> VriskoContact(companyName,query,webSite,description,address,category,telephone.trim,email.trim)
        }
      }
      step += 1
      if (freeBoxes.size() > 0) {
        val iter = freeBoxes.iterator()
        while (iter.hasNext) {
          freeStep += 1
          subStep = 0
          val box = iter.next()
          subStep += 1
          val companyName = box.getAllElementsByClass("CompanyName").iterator().next().getTextExtractor.toString
          subStep += 1
          val description = box.getAllElementsByClass("CompanyDescr").iterator().next().getTextExtractor.toString
          subStep += 1
          val websites = box.getAllElementsByClass("AdvSiteWebSite")
          subStep += 1
          var webSite = if (websites.size() == 0) "" else websites.iterator().next().getAttributes.get("href").getValue
          subStep += 1
          if (webSite.trim == "http://") webSite = ""
          subStep += 1
          val address = box.getAllElementsByClass("FreeListingAddress").iterator().next().getTextExtractor.toString
          subStep += 1
          val categoryBox = box.getAllElementsByClass("FreeListingCategoryArea")
          val category = if (categoryBox.size() > 0 ) categoryBox.iterator().next().getTextExtractor.toString else ""
          subStep += 1


          val telephones = new mutable.HashSet[String]
          val tIter = box.getAllElements("itemprop","telephone",true).iterator()
          while (tIter.hasNext) {
            telephones += tIter.next().getTextExtractor.toString
          }
          var telephone = ""
          telephones.foreach(e=> telephone += e + " ")
          val emails = new mutable.HashSet[String]()
          val eIter = box.getAllElements("itemprop","email",true).iterator()
          while (eIter.hasNext) {
            val element = eIter.next()
            val hrefPart = element.getAttributeValue("href")
            if (hrefPart != null) {
              if (hrefPart.contains(":") && hrefPart.contains("www.vrisko.gr/email/") == false)
                emails += hrefPart.split(":")(1).trim
            }
          }
          var email = ""
          emails.foreach(e => email +=  e + " ")
          contacts += companyName -> VriskoContact(companyName,query,webSite,description,address,category,telephone.trim,email.trim)
        }
      }

    }catch {
      case e:Exception => {
        if (step < 3)
          logger.error("Error while extracting information from url: " + urlString + " with Exception e:(" + e + ") at step " + step)
        else if (step == 3)
          logger.error("Error while extracting information from url: " + urlString + " with Exception e:(" + e + ") at advStep " + advStep + ":" + subStep)
        else
          logger.error("Error while extracting information from url: " + urlString + " with Exception e:(" + e + ") at freeStep " + freeStep + ":" + subStep)
      }
    }

  }

  def saveJson(fileName:String): Unit = {
    implicit val formats = DefaultFormats
    val m = contacts.map(e => e._2).toList
    val outPutjson = ("contacts" -> m.map { e =>
      (("name" -> e.name)) ~
        (("query" -> e.query)) ~
        (("website" -> e.website)) ~
        (("description" -> e.description)) ~
        (("address" -> e.address)) ~
        (("category" -> e.category)) ~
        (("telephones" -> e.telephones)) ~
        (("email" -> e.email))
    })



    val fw = new BufferedWriter(new FileWriter(new File(fileName)))
    fw.write(prettyRender(outPutjson))
    fw.close()
  }

}

case class VriskoContact (name:String,query:String,website:String,description:String,address:String,category:String, telephones:String, email:String)
