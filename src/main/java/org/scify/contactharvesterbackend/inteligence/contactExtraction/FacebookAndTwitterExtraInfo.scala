package org.scify.contactharvesterbackend.inteligence.contactExtraction

import java.io._
import java.net.{URL, URLConnection}

import net.htmlparser.jericho.{Element, Source}
import net.liftweb.json.JsonDSL._
import net.liftweb.json._
import org.apache.logging.log4j.LogManager

import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

/**
  * Created by akosmo on 17/3/2016.
  */
class FacebookAndTwitterExtraInfo {

  var contactsPerLink:List[ContactsPerLink] = Nil
  var contactsPerLinkExpanded:List[ContactsPerLinkExpanded] = Nil
  private val logger = LogManager.getRootLogger

  def loadFromJsonFile(fileName:String): Unit = {
    val fr = new FileReader(fileName)
    val br = new BufferedReader(fr)
    val jsonStringBuffer = new StringBuffer()
    var line = br.readLine()
    while (line != null) {
      jsonStringBuffer.append(line)
      line = br.readLine()
    }
    br.close()
    fr.close()
    implicit val formats = DefaultFormats
    val json = JsonParser.parse(jsonStringBuffer.toString)
    contactsPerLink = json.extract[Map[String,List[ContactsPerLink]]].head._2
  }

  def printContactsPerLink(): Unit ={
    contactsPerLink.foreach(e=>{
      println(e.url)
      println("Facebook:")
      e.facebook.foreach(e => println(" " + e))
      println("Twitter:")
      e.twitter.foreach(e => println(" " + e))
      println("Linkedin:")
      e.linkedin.foreach(e => println(" " + e))
    })
  }

  def printContactsPerLinkExpanded(): Unit ={
    contactsPerLinkExpanded.foreach(e=>{
      println(e.url)
      println("Facebook:")
      e.facebook.foreach(e => {
        println(" " + e.facebookUrl)
        println(" information:")
        e.information.foreach(info => println("  " + info))
      })
      println("Twitter:")
      e.twitter.foreach(e => {
        println(" " + e.twitterUrl)
        println(" location: " + e.location)
        println(" url: " + e.url)
      })
      println("Linkedin:")
      e.linkedin.foreach(e => println(" " + e))
    })
  }

  def expand (): Unit = {
    facebookBuffer.clear()
    twitterBuffer.clear()
    contactsPerLinkExpanded = Nil
    var counter = contactsPerLink.size
    contactsPerLink.foreach(contact => {
      logger.info("Contacts Left:" + counter)
      counter -= 1
      val url = contact.url

      var facebook:List[FacebookInfo] = Nil
      val indexOfAfter = "https://www.facebook.com/pages/".length
      contact.facebook.filter(e => e.contains("posts") == false).foreach(e=> {
        var name = e
        val info = new mutable.HashMap[String,String]()
        if (name.contains("http://"))
          name = "https://" + name.substring(7)
        if (name.contains("/pages/")) {
          val before = "https://www.facebook.com/"
          val after = name.substring(indexOfAfter)
          val s = after.split("/")
          val alteredAfter = s(0) + "-" + s(1)
          name =  before + alteredAfter
        }

        name = if (name.charAt(name.length-1) == '/') name + "info?tab=page_info" else name + "/info?tab=page_info"

        def operation(uRLConnection: URLConnection):Boolean = {
          val html = new Source(uRLConnection)

          val infoPart = html.getAllElementsByClass("fbSettingsList").iterator().next().getChildElements
          val iter = infoPart.iterator()
          while (iter.hasNext) {
            val element = iter.next()

            val partsIter = element.getAllElementsByClass("_50f4").iterator()

            val key = partsIter.next().getTextExtractor.toString
            val value = partsIter.next().getTextExtractor.toString

            info += key -> value
          }
          true
        }

        facebookBuffer.get(name) match {
          case Some(x) => {
            if (x != null) {
              facebook = x :: facebook
            }
          }
          case None => {
            try {
              val url = new URL(name)
              val con = url.openConnection()
              con.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0")
              implicit val ec = ExecutionContext.global
              val f = Future{operation(con)}
              Await.result(f,Duration.apply("10 seconds"))
              val fi = FacebookInfo(name,info.toMap)
              facebook = fi :: facebook
              facebookBuffer += name -> fi
            }catch {
              case e:Exception => {
                logger.info("Problem with url " + name + " with exception + " + e)
                facebookBuffer += name -> null
              }
            }
            Thread sleep(1000)
          }
        }
      })
      var twitter:List[TwitterInfo] = Nil
      contact.twitter.foreach(e=> {
        var name = e
        if (name.contains("http://"))
          name = "https://" + name.substring(7)
        name = name.replaceAll("/#!","")
        var location = ""
        var url_info = ""

        def operationLoc(html:Element):Boolean = {
          val loc = html.getAllElementsByClass("ProfileHeaderCard-location").iterator().next().getTextExtractor.toString
          location = loc
          true
        }

        def operationUrl(html:Element):Boolean = {
          val url = html.getAllElementsByClass("ProfileHeaderCard-urlText").iterator().next().getTextExtractor.toString
          url_info = url
          true
        }

        twitterBuffer.get(name) match {
          case Some(x) => {
            if (x != null) {
              twitter = x :: twitter
            }
          }
          case None => {
            try {
              val url = new URL(name)
              val con = url.openConnection()
              con.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0")
              val html = new Source(con).getElementById("page-container")
              implicit val ec = ExecutionContext.global
              var f = Future{operationLoc(html)}
              Await.result(f,Duration.apply("10 seconds"))
              f = Future{operationUrl(html)}
              Await.result(f,Duration.apply("10 seconds"))
              val ti = TwitterInfo(name,location,url_info)
              twitter =  ti :: twitter
              twitterBuffer += name -> ti
            }catch {
              case e:Exception => {
                logger.info("Problem with url " + name + " with exception + " + e)
                twitterBuffer += name -> null
              }
            }
            Thread sleep(1000)
          }
        }
      })
      val linkedin = contact.linkedin
      val expanded = ContactsPerLinkExpanded(url,facebook,twitter,linkedin)
      contactsPerLinkExpanded = expanded :: contactsPerLinkExpanded
    })
  }

  def writeExpandedToFile (fileName:String): Unit = {
    val json =
      ("contactsPerLink" ->
        contactsPerLinkExpanded.map { e =>
          (("url" -> e.url) ~
            ("facebook" -> e.facebook.map { fe =>
              (("facebookUrl" -> fe.facebookUrl) ~
                ("information" -> fe.information))
            }) ~
            ("twitter" -> e.twitter.map { te =>
              (("twitterUrl" -> te.twitterUrl) ~
                ("location" -> te.location) ~
                ("url" -> te.url))

            }) ~
            ("linkedin" -> e.linkedin))
        })
    val fw = new BufferedWriter(new FileWriter(new File(fileName)))
    fw.write(prettyRender(json))
    fw.close()
  }

  private val facebookBuffer = new mutable.HashMap[String,FacebookInfo]()
  private val twitterBuffer = new mutable.HashMap[String,TwitterInfo]()

}

case class ContactsPerLink(url:String,facebook:List[String],twitter:List[String],linkedin:List[String])

case class ContactsPerLinkExpanded(url:String,facebook:List[FacebookInfo],twitter:List[TwitterInfo],linkedin:List[String])

case class ContactsWithQueries(url:String, queries: List[String],facebook:List[FacebookInfo],twitter:List[TwitterInfo],linkedin:List[String])

case class TwitterInfo(twitterUrl:String,location:String,url:String)

case class FacebookInfo(facebookUrl:String,information:Map[String,String])

case class ContactsPerLinkWithQueries(url:String, queries:List[String],facebook:List[FacebookInfo],twitter:List[TwitterInfo],linkedin:List[String])
