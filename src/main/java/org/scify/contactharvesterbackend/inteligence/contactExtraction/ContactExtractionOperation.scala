package org.scify.contactharvesterbackend.inteligence.contactExtraction

import java.io._
import java.net.URL
import java.util

import akka.actor.{Actor, ActorLogging, ActorRef, PoisonPill}
import org.apache.logging.log4j.LogManager
import org.scify.contactharvesterbackend.basic.{CancelOperationMessage, Settings, StartOperationMessage}
import org.scify.contactharvesterbackend.bean.{ExtractedContact, ExtractedContacts}
import org.scify.contactharvesterbackend.enumerations.SearchStatus
import org.scify.contactharvesterbackend.inteligence.{AlterSearchDetails, JsonTool, RandomTool, UrlCrawler}
import org.springframework.data.mongodb.core.MongoTemplate

import scala.collection.JavaConversions._
import scala.collection.mutable.{HashMap, HashSet, ListBuffer}

/**
  * Created by akosmo on 22/4/2016.
  */
class ContactExtractionOperation(searchKeeperActor: ActorRef, contactExtractorActor:ActorRef,userId: Int, searchId: Int, keywords: java.util.List[String],maxTopResultsPerSearch:Int, extraStringPerSearch:String, mongoTemplate: MongoTemplate) extends Actor with ActorLogging {
  private val exContacts = new ExtractedContacts(userId, searchId, keywords,maxTopResultsPerSearch,extraStringPerSearch)
  private var canceled = false
  private val folder = Settings.dataFolder + userId + "_" + searchId + "/"
  private val folderFile = new File(folder)
  private var links = 0
  private val logger = LogManager.getRootLogger
  private var useVrisko = false



  private var crawledPages = ""


  private def delete(file:File) {
    if (file.isDirectory) {
      file.listFiles().foreach(e => delete(e))
    }
    file.delete()
  }

  def receive: Receive = {
    case StartOperationMessage => initiateExtraction()
    case CoExOpBingSearchMessage => bingSearch()
    case CoExOpCrawlPagesMessage => crawlPages()
    case CoExOpExtractFromCrawledPagesMessage => extract()
    case CoExOpAugmentResultsMessage => augment()
    case CoExOpAddQueriesMessage => addQueries()
    case CoExOpVriskoMessage => vrisko()
    case CoExOpAddToMongoMessage => addToMongo()
    case CancelOperationMessage => cancel()
    case _ => LogManager.getRootLogger.info("Wrong message at KeywordExtractionOperation")
  }

  private def initiateExtraction(): Unit = {
    if (folderFile.exists()) {
      delete(folderFile)
      folderFile.mkdir()
    }else {
      folderFile.mkdir()
    }
    mongoTemplate.save(exContacts)
    searchKeeperActor ! AlterSearchDetails(userId,searchId,10,SearchStatus.running,null)
    self ! CoExOpBingSearchMessage
  }

  private def bingSearch (): Unit = {
    val bs = new BingSearch(Settings.bingKey)
    if (extraStringPerSearch.contains("loc:gr"))
      useVrisko = true
    logger.info("Initiating bing search for userId: " + userId + " and searchId: " + searchId + " with keywords " + keywords + ", extraStringPerSearch: " + extraStringPerSearch + " and maxTopResultsPerSearch: " + maxTopResultsPerSearch)
    val searchTool = new SearchEngineTool(keywords,bs,extraStringPerSearch,maxTopResultsPerSearch)
    val bingResults = new File (folder + "bingResults")
    bingResults.mkdir()
    val urlsFile = new File(folder + "urls")
    val fw: FileWriter = new FileWriter(urlsFile)
    val bw: BufferedWriter = new BufferedWriter(fw)
    val results: util.HashSet[String] = searchTool.getSearchEngineLinks(folder + "bingResults/")
    results.foreach(url => {bw.write(url);bw.newLine()})
    bw.flush
    bw.close
    fw.close
    links = results.size()
    searchKeeperActor ! AlterSearchDetails(userId,searchId,20,SearchStatus.running,null)
    self ! CoExOpCrawlPagesMessage
  }

  private def crawlPages (): Unit = {
    logger.info("Initiating crawling for userId: " + userId + " and searchId: " + searchId)
    val contactCrawlerResults = folder + "contactCrawlerResults"
    val contFile = new File (contactCrawlerResults)
    if (contFile.exists() == false)
      contFile.mkdir()

    val cr = new UrlCrawler
    cr.crawlPagesToFile(folder + "urls",contactCrawlerResults)
    searchKeeperActor ! AlterSearchDetails(userId,searchId,30,SearchStatus.running,null)
    self ! CoExOpExtractFromCrawledPagesMessage
  }

  private def extract (): Unit ={
    logger.info("Initiating extracting for userId: " + userId + " and searchId: " + searchId)
    val ce  = new ContactExtractionTool
    ce.extractContacts(folder + "urls", folder + "contactCrawlerResults/", links, folder + "outputJson.txt")
    searchKeeperActor ! AlterSearchDetails(userId,searchId,50,SearchStatus.running,null)
    self ! CoExOpAugmentResultsMessage
  }

  private def augment(): Unit = {
    logger.info("Initiating augmenting for userId: " + userId + " and searchId: " + searchId)
    val extraInfo: FacebookAndTwitterExtraInfo = new FacebookAndTwitterExtraInfo
    extraInfo.loadFromJsonFile(folder+"outputJson.txt")
    extraInfo.expand
    extraInfo.writeExpandedToFile(folder + "outputJsonExpanded.txt")
    searchKeeperActor ! AlterSearchDetails(userId,searchId,60,SearchStatus.running,null)
    self ! CoExOpAddQueriesMessage
  }

  private def addQueries (): Unit = {
    logger.info("Initiating queries expansion for userId: " + userId + " and searchId: " + searchId)
    val linkToQueries: util.HashMap[String, util.HashSet[String]] = new util.HashMap[String, util.HashSet[String]]
    val bingResultsDirectory: File = new File(folder + "bingResults")
    val bingResultsFiles: Array[File] = bingResultsDirectory.listFiles
    bingResultsFiles.foreach(file => {
      if (file.isFile) {
        val fr: FileReader = new FileReader(file)
        val br: BufferedReader = new BufferedReader(fr)
        val lines: StringBuffer = new StringBuffer
        var line: String = br.readLine
        while (line != null) {
          lines.append(line)
          line = br.readLine
        }
        br.close
        fr.close
        val query: String = file.getName
        val links: util.ArrayList[String] = JsonTool.extract(lines.toString)
        links.foreach(link => {
          if (linkToQueries.containsKey(link)) {
            linkToQueries.get(link).add(query)
          }
          else {
            val queries: util.HashSet[String] = new util.HashSet[String]
            queries.add(query)
            linkToQueries.put(link, queries)
          }
        })
      }
    })
    JsonTool.addQueriesToJsonFile(folder + "outputJsonExpanded.txt", linkToQueries)
    searchKeeperActor ! AlterSearchDetails(userId,searchId,65,SearchStatus.running,null)
    if (useVrisko)
      self ! CoExOpVriskoMessage
    else
      self ! CoExOpAddToMongoMessage
  }

  def vrisko ():Unit = {
    logger.info("Initiating vrisko search for userId: " + userId + " and searchId: " + searchId)
    val ex: ExtractVriskoPagesUsingKeywords = new ExtractVriskoPagesUsingKeywords(keywords)
    val extractedUrls: HashMap[String, HashSet[String]] = ex.extractUrls
    searchKeeperActor ! AlterSearchDetails(userId,searchId,75,SearchStatus.running,null)
    logger.info("Vrisko search (links gathered) for userId: " + userId + " and searchId: " + searchId)
    val vs = new VriskoSearch
    extractedUrls.foreach(e => {
      val query = e._1
      e._2.foreach(link => {
        vs.extractForUrl(link,query)
        val sleepValue = 5000 + (RandomTool.generateProb() * 1000).toInt
        Thread.sleep(sleepValue)
      })
    })
    logger.info("Contacts extracted from Vrisko: " + vs.contacts.size)
    vs.saveJson(folder + "vrisko.json")
    searchKeeperActor ! AlterSearchDetails(userId,searchId,90,SearchStatus.running,null)
    self ! CoExOpAddToMongoMessage
  }

  def addToMongo (): Unit = {
    //add contacts from bing search
    logger.info("Initiating addToMongo for userId: " + userId + " and searchId: " + searchId)
    val bingResults = JsonTool.readContactsWithQueriesFromJson(folder + "outputJsonExpanded_withQueries.txt")
    val contacts = exContacts.getContacts
    bingResults.foreach(e => {
      contacts.add(changeBingJsonToFinal(e))
    })

    //add contacts from vrisko
    if (useVrisko) {
      val vriskoResults = JsonTool.readVriskoJson(folder + "vrisko.json")
      vriskoResults.foreach(e => {
        contacts.add(changeVriskoJsonToFinal(e))
      })
    }

    //remove dublicates and add Id
    try {
      var contactIdCounter = 1
      val newListOfContacts = new util.ArrayList[ExtractedContact]()
      val contactsUsed = Array.fill[Boolean](contacts.size())(false)
      for (i <- 0 until contacts.size()) {
        if (contactsUsed(i) == false) {
          contactsUsed(i) = true
          val contact = contacts.get(i)
          val domain = contact.getDomain
          val similarContacts = new ListBuffer[ExtractedContact]
          similarContacts += contact
          //gather similar contacts
          for (j <- (i + 1) until contacts.size()) {
            if (contactsUsed(j) == false) {
              val contact = contacts.get(j)
              if (contact.getDomain.equals(domain)) {
                similarContacts += contact
                contactsUsed(j) = true
              }
            }
          }
          //create New Contact
          val contactId = contactIdCounter
          contactIdCounter += 1
          val email = gatherUniqueElements(similarContacts.map(e => e.getEmail))
          val phones = gatherUniqueElements(similarContacts.map(e => e.getPhone))
          val address = gatherUniqueElements(similarContacts.map(e => e.getAddress))
          val facebookURL = gatherUniqueElements(similarContacts.map(e => e.getFacebookURL))
          val twitterURL = gatherUniqueElements(similarContacts.map(e => e.getTwitterURL))
          val additionalInfo = gatherUniqueElements(similarContacts.map(e => e.getAdditionalInfo))
          val relatedQueries = gatherUniqueElements(similarContacts.map(e => e.getRelatedQueries))
          newListOfContacts.add(new ExtractedContact(domain, email, phones, address, facebookURL, twitterURL, additionalInfo, relatedQueries, contactId))
        }
      }
      exContacts.setContacts(newListOfContacts)

      //save and finalize
      mongoTemplate.save(exContacts)
      searchKeeperActor ! AlterSearchDetails(userId, searchId, 100, SearchStatus.completed, null)
      contactExtractorActor ! ContactExtractionOperationTerminatedMessage(userId + ":" + searchId)
      self ! PoisonPill
    }catch {
      case e:Exception => logger.error("Exception: (" + e + ")")
    }
  }

  private def gatherUniqueElements(elements:ListBuffer[String]):String = {
    val uniqueElements = new HashSet[String]
    elements.foreach(e=>{if (e != null) uniqueElements += e.trim})
    if (uniqueElements.size == 0)
      ""
    else {
      var ret = uniqueElements.head
      uniqueElements.tail.foreach(e=> ret += ", " + e)
      ret
    }
  }

  private def cancel (): Unit = {
    if (canceled == false) {
      canceled = true
      searchKeeperActor ! AlterSearchDetails(userId, searchId, 50, SearchStatus.canceled, null)
      contactExtractorActor ! ContactExtractionOperationTerminatedMessage(userId + ":" + searchId)
      self ! PoisonPill
    }
  }

  private def changeBingJsonToFinal (contact:ContactsWithQueries):ExtractedContact = {
    val ret = new ExtractedContact()

    //domain
    if (contact.facebook.size > 0) {
      contact.facebook.head.information.get("Ιστότοπος") match {
        case Some(x) => ret.setDomain(x.trim)
        case None => {
          if (contact.twitter.size > 0) {
            ret.setDomain(contact.twitter.head.url.trim)
          }else {
            val url = new URL (contact.facebook.head.facebookUrl)
            ret.setDomain(url.getHost.trim)
          }
        }
      }
      contact.facebook.head.information.get("Email") match {
        case Some(x) => ret.setEmail(x.trim)
        case None =>;
      }
      contact.facebook.head.information.get("Τηλέφωνο") match {
        case Some(x) => ret.setPhone(x.trim)
        case None =>;
      }
      contact.facebook.head.information.get("Σύντομη περιγραφή") match {
        case Some(x) => ret.setAdditionalInfo(x.trim)
        case None =>;
      }
      ret.setFacebookURL(contact.facebook.head.facebookUrl)
    }else if (contact.twitter.size > 0) {
      ret.setDomain(contact.twitter.head.url.trim)
    }else {
      val url = new URL (contact.url)
      ret.setDomain(url.getHost.trim)
    }

    if (contact.twitter.size > 0)
      ret.setTwitterURL(contact.twitter.head.twitterUrl.trim)


    val keywords = new collection.mutable.HashSet[String]
    exContacts.getKeywords.foreach(keyword => {
      contact.queries.foreach(query => {
        if (query.contains(keyword)) {
          keywords += keyword
        }
      })
    })
    val keywordsString = new StringBuffer()
    keywords.foreach(e => keywordsString.append(e + " "))
    ret.setRelatedQueries(keywordsString.toString)

    ret
  }

  private def changeVriskoJsonToFinal (contact:VriskoContact):ExtractedContact = {
    val ret = new ExtractedContact()

    val name = contact.name.trim
    if (name == "")
      ret.setDomain(contact.website)
    else
      ret.setDomain(name)
    ret.setAddress(contact.address)
    ret.setRelatedQueries(contact.query)
    ret.setAdditionalInfo(contact.description + " -- " + contact.category)
    ret.setPhone(contact.telephones)
    ret.setEmail(contact.email)
    ret
  }

  case object CoExOpBingSearchMessage
  case object CoExOpCrawlPagesMessage
  case object CoExOpExtractFromCrawledPagesMessage
  case object CoExOpAugmentResultsMessage
  case object CoExOpAddQueriesMessage
  case object CoExOpVriskoMessage
  case object CoExOpAddToMongoMessage
}


