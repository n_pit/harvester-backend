package org.scify.contactharvesterbackend.inteligence.contactExtraction

import java.io._

import net.liftweb.json.JsonDSL._
import net.liftweb.json.prettyRender
import org.scify.contactharvesterbackend.inteligence.UrlCrawler

import scala.collection.mutable.{ArrayBuffer, HashSet}

/**
  * Created by akosmo on 1/2/2016.
  */
class ContactExtractionTool {
  private val crawler = new UrlCrawler

  def extractContacts (fileWithUrls:String, contactCrawlerResults:String,untilIndex:Int, output:String): Unit = {
    val file = new File(fileWithUrls)
    if (file.exists()) {
      val fr = new FileReader(file)
      val br = new BufferedReader(fr)
      var line = br.readLine()
      val links = new ArrayBuffer[String]
      while (line != null) {
        links += line
        line = br.readLine()
      }
      br.close()
      fr.close()
      val extractedContacts = extractContacts(links,contactCrawlerResults,untilIndex).filter(e => e.facebook.size + e.twitter.size + e.linkedIn.size > 0)
      val json =
        ("contactsPerLink" ->
          extractedContacts.map { e =>
            (("url" -> e.link) ~
              ("facebook" -> e.facebook) ~
              ("twitter" -> e.twitter) ~
              ("linkedin" -> e.linkedIn)
              )
          })
      val fw = new BufferedWriter(new FileWriter(new File(output)))
      fw.write(prettyRender(json))
      fw.close()
    }
  }

  private def extractContacts (urls:ArrayBuffer[String], contactCrawlerResults:String,untilIndex:Int):ArrayBuffer[Contacts] = {
    val ret = new ArrayBuffer[Contacts]()
    for (i <- 0 until untilIndex) {
      val url = urls(i)
      val contacts = crawler.extractContactInfoFromFile(contactCrawlerResults + i,url)
      if (contacts != null) {
        ret += contacts
      }
    }
    ret
  }

}

case class Contacts (link:String) {
  val emails = new HashSet[String]
  val linkedIn = new HashSet[String]
  val twitter = new HashSet[String]
  val facebook = new HashSet[String]
}
