package org.scify.contactharvesterbackend.inteligence.contactExtraction

import java.net.{URL, URLEncoder}

import net.htmlparser.jericho.Source
import org.apache.logging.log4j.LogManager
import org.scify.contactharvesterbackend.inteligence.RandomTool

import scala.collection.JavaConversions._
import scala.collection.mutable.{HashMap, HashSet}

/**
  * Created by akosmo on 5/5/2016.
  */
class ExtractVriskoPagesUsingKeywords (keywords:java.util.List[String]) {
  private val vrisko = "http://www.vrisko.gr/search/"
  private val logger = LogManager.getRootLogger

  def extractUrls():HashMap[String,HashSet[String]] = {
    val ret = new HashMap[String,HashSet[String]]
    keywords.foreach(keyword => {
      val extractedUrls = extractUrls(keyword)
      val sleepValue = 5000 + (RandomTool.generateProb() * 1000).toInt
      Thread.sleep(sleepValue)
      ret += keyword -> extractedUrls
    })
    ret
  }

  def test (s:String): Unit = {
    val encoded = URLEncoder.encode(s, "UTF-8")
    println(encoded)
  }

  private def extractUrls(keyword: String): HashSet[String] = {
    val ret = new HashSet[String]
    try {
      val url = new URL(vrisko + URLEncoder.encode(keyword.replaceAll(" ","-"), "UTF-8") + "/")
      val con = url.openConnection()
      con.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0")
      val html = new Source(con)
      html.getElementById("pagerPlaceHolder").getAllElementsByClass("pageLink").foreach(e => {
        val link = e.getAttributes.get("href").getValue
        val beforeSearch = "http://www.vrisko.gr/search/"
        val afterSearch = link.substring(beforeSearch.length)
        val s = afterSearch.split("/\\?page=")// \\ is escape for ?
        val beforePage = URLEncoder.encode(s(0), "UTF-8")
        val afterPage = "/?page=" + s(1)
        ret += (beforeSearch + beforePage + afterPage)
      })
      if (ret.size == 0)
        ret += (vrisko + URLEncoder.encode(keyword.replaceAll(" ","-"), "UTF-8") + "/")

      ret
    }catch {
      case e:Exception => {
        logger.info("Crawling from vrisko exception: (" + e + ")")
        ret
      }
    }
  }
}
