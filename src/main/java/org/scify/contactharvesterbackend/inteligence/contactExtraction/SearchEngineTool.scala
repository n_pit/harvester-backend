package org.scify.contactharvesterbackend.inteligence.contactExtraction

import java.io.{BufferedWriter, FileWriter}
import java.util

import net.liftweb.json._
import org.apache.logging.log4j.LogManager

import scala.collection.JavaConversions._
import scala.collection.mutable.ListBuffer

/**
  * Created by akosmo on 29/1/2016.
  */
class SearchEngineTool(terms:util.List[String], bingSearch: BingSearch, extraSearchingParameters:String, topResultsPerSearch:Int) {

  private val logger = LogManager.getRootLogger

  def getAllPairsWithContact ():ListBuffer[String] = {
    val ret = new ListBuffer[String]
    for (i <- 0 until terms.size()) {
      val term1 = terms(i)
      for (j <- (i+1) until terms.size()) {
        val term2 = terms(j)
        val searchString = term1 + " " + term2 + " " + extraSearchingParameters
        logger.info("Bing Searching for keywords with String: " + searchString)
        ret += searchString
      }
    }
    ret
  }

  def getSearchEngineLinks (bingResults:String): util.HashSet[String] = {
    val ret = new util.HashSet[String]()
    val pairs = getAllPairsWithContact()
    var pairsLeft = pairs.size

    pairs.foreach(e => {
      pairsLeft -= 1
      val fw = new FileWriter(bingResults + e)
      val wb = new BufferedWriter(fw)
      val result = bingSearch.search(e)
      val json = parse(result)
      wb.write(prettyRender(json))
      var links = compactRender(json \\ "Url").toString.substring(1)
      links = links.substring(0,links.length-1)
      val urlEntries = links.split("\",\"")
      var counter = 0
      urlEntries.foreach(e => {
        counter += 1
        if (counter < topResultsPerSearch) {
          val link = e.substring(e.lastIndexOf('"') + 1)
          ret.add(link)
        }
      })
      wb.flush()
      wb.close()
      fw.close()
      Thread.sleep(2000)
    })
    ret
  }
}
