package org.scify.contactharvesterbackend.inteligence.classification

import java.util.regex.Pattern

import org.scify.contactharvesterbackend.basic.Settings
import org.tartarus.snowball.ext.englishStemmer
/**
  * Created by akosmo on 3/6/2016.
  */
class StemmersAndStopWordsParser {
  private val greekStopWords = Settings.greekStopWords
  private val englishStopWords = Settings.englishStopWords
  private val greekStemmer = new GreekStemmer(greekStopWords)
  private val englishStemmer = new englishStemmer
  private val greekPatern = Pattern.compile("[α-ωάέίόώήύϊ]");

  def stem (word:String):String = {
    val w = word.toLowerCase.replaceAll("[^α-ωa-z0-9άέίόώήύϊ ]", "")
    if (w == "")
      return ""
    val m = greekPatern.matcher(w)
    if (m.find()) {
      greekStemmer.stem(w)
    }else {
      if (englishStopWords.contains(w))
        ""
      else {
        englishStemmer.setCurrent(w)
        englishStemmer.stem()
        englishStemmer.getCurrent
      }
    }
  }
}
