package org.scify.contactharvesterbackend.inteligence.classification

import java.util

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import org.apache.logging.log4j.LogManager
import org.scify.contactharvesterbackend.basic.{CancelOperationMessage, Settings, StartOperationMessage}
import org.springframework.data.mongodb.core.MongoTemplate

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  * Created by akosmo on 22/4/2016.
  */
class ContactClassifier (searchKeeperActor: ActorRef,actorSystem:ActorSystem, mongoTemplate: MongoTemplate) extends Actor with ActorLogging {

  private val actorOperationsMap = new mutable.HashMap[String,ActorRef]()
  private var pendingOperations = new ListBuffer[ActorRef]
  private val max = Settings.maxContactClassifications
  private var activeSearches = 0
  private val stemmer = new StemmersAndStopWordsParser

  def receive: Receive = {
    case StartContactClassificationMessage(userId,searchId,positive,negative) => {
      val key = userId + ":" + searchId
      val props: Props = Props(new ContactClassificationOperation(searchKeeperActor,self,userId,searchId,positive,negative, stemmer,mongoTemplate))
      val actor = actorSystem.actorOf(props)

      actorOperationsMap += key -> actor
      if (activeSearches < max) {
        actor ! StartOperationMessage
        activeSearches += 1
      }else {
          pendingOperations += actor
      }
    }

    case CancelContactsClassificationMessage(userId,searchId) => {
      val key = userId + ":" + searchId
      actorOperationsMap.get(key) match {
        case Some(x) => x ! CancelOperationMessage
        case None => LogManager.getRootLogger.info("No classification contacts operation with userId:" + userId + " and searchId:" + searchId)
      }
    }
    case ContactClassificationOperationTerminatedMessage(key) => {
      actorOperationsMap.remove(key)
      if (pendingOperations.size == 0)
        activeSearches -= 1
      else {
        val actor = pendingOperations.head
        pendingOperations = pendingOperations.tail
        actor ! StartOperationMessage
      }
    }
  }
}
case class StartContactClassificationMessage(userId: Int, searchId: Int, positive:util.List[Integer], negative:util.List[Integer])
case class CancelContactsClassificationMessage(userId: Int, searchId: Int)
case class ContactClassificationOperationTerminatedMessage(key:String)