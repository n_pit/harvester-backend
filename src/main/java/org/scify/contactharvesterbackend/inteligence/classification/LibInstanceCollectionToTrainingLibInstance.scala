package org.scify.contactharvesterbackend.inteligence.classification

import org.scify.pipeline.basic.{Note, ProgressStatus}
import org.scify.pipeline.classificationcomponent.{LibInstance, LibInstanceCollection, StringLexicon}
import org.scify.pipeline.interfaces._
/**
  * Created by akosmo on 3/6/2016.
  */
class LibInstanceCollectionToTrainingLibInstance(var name: String, var noteReceiver: INoteReceiver) extends IPipeLineComponent[LibInstanceCollection, LibInstanceCollection] {

  private var input: LibInstanceCollection = null
  private var output: LibInstanceCollection = null


  override def execute(): Note = {
    if (input == null)
      return new Note(getSender, "No input set", ProgressStatus.Error)
    else {
        output = new LibInstanceCollection
        val iter = input.iterator()
        while (iter.hasNext) {
          val instance = iter.next()
          val cat = instance.categories.head
          if (cat == 1 || cat == -1) {
            output.add(instance)
          }
        }
        new Note(getSender, "Created", ProgressStatus.Completed)
    }
  }

  override def getOutputData: LibInstanceCollection = output

  override def setInputData(input: LibInstanceCollection): Unit = this.input = input

  override def get(): LibInstanceCollection = output

  override def load(fileName: String): Note = {
    output.loadFromSparseVectorFile(fileName)
    new Note(getSender, "Loaded successfully", ProgressStatus.Completed)
  }

  override def save(fileName: String): Note = {
    output.saveToSparseVectorFile(fileName)
    new Note(getSender, "Saved successfully", ProgressStatus.Completed)
  }

  override def setSender(name: String): Unit = this.name = name

  override def getSender: String = name

  override def setINoteReceiver(iNoteReceiver: INoteReceiver): Unit = noteReceiver = iNoteReceiver

  override def sendNote(note: Note): Unit = {
    noteReceiver.add(note)
  }
}
