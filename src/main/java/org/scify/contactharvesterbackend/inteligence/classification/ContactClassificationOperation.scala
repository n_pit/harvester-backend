package org.scify.contactharvesterbackend.inteligence.classification

import java.io.File
import java.util

import akka.actor.{Actor, ActorLogging, ActorRef, PoisonPill}
import org.apache.logging.log4j.LogManager
import org.scify.contactharvesterbackend.basic.{CancelOperationMessage, Settings, StartOperationMessage}
import org.scify.contactharvesterbackend.bean.{ClassificationInfoPerInstance, ExtractedContacts, ExtractedContactsClassificationSequence}
import org.scify.contactharvesterbackend.enumerations.SearchStatus
import org.scify.contactharvesterbackend.inteligence.AlterSearchDetails
import org.scify.contactharvesterbackend.inteligence.contactExtraction.ContactExtractionOperationTerminatedMessage
import org.scify.pipeline.basic.{BasicINoteReceiver, PipeLine, ProgressStatus}
import org.scify.pipeline.classificationcomponent._
import org.springframework.data.mongodb.core.MongoTemplate

import collection.mutable.{HashSet, ListBuffer}
import collection.JavaConversions._
/**
  * Created by akosmo on 3/6/2016.
  */
class ContactClassificationOperation (searchKeeperActor: ActorRef, contactClassificationActor:ActorRef,userId: Int, searchId: Int,pos:util.List[Integer],neg:util.List[Integer], stemmer:StemmersAndStopWordsParser,mongoTemplate: MongoTemplate) extends Actor with ActorLogging {
  private val exContacts = mongoTemplate.findById("ExtractedContacts" + userId + ":" + searchId,classOf[ExtractedContacts])
  private val classifiedContacts = new ExtractedContactsClassificationSequence(userId,searchId,pos,neg)
  private var canceled = false
  private val folder = Settings.dataFolder + userId + "_" + searchId + "/"
  private val folderFile = new File(folder + "classification/")
  private val logger = LogManager.getRootLogger
  private val positive = new HashSet[Int]
  pos.foreach(e=> positive += e)
  private val negative = new HashSet[Int]
  neg.foreach(e=> negative += e)
  //classification staff
  private val notificationReceiver = new BasicINoteReceiver
  private val stemLex = new StringLexicon
  private var allLibInstances:LibInstanceCollection = null
  private val trainingComponent = new LibLinearTrainingComponent("training",LibLinearClassifierBuilder.getRidgeLogisticRegression(),notificationReceiver)
  private val predictionComponent = new LibLinearTestComponent("predicting",notificationReceiver)



  def receive: Receive = {
    case StartOperationMessage => initiateExtraction()
    case CoClOpCreateLibInstances => createLibInstances()
    case CoClOpTrainModels => train()
    case CoClOpPredict => predict()
    case CoClOpAddToMongo => addToMongo()
    case CancelOperationMessage => cancel()
    case _ => LogManager.getRootLogger.info("Wrong message at ContactClassificationOperation")
  }

  private def initiateExtraction(): Unit = {
    if (folderFile.exists()) {
      delete(folderFile)
      folderFile.mkdir()
    }else {
      folderFile.mkdir()
    }
    mongoTemplate.save(classifiedContacts)
    searchKeeperActor ! AlterSearchDetails(userId,searchId,10,SearchStatus.running,null)
    self ! CoClOpCreateLibInstances
  }

  private def createLibInstances (): Unit = {
    val pipeline = new PipeLine("PipeLineProcedure", folder + "classification/")
    pipeline.setINoteReceiver(notificationReceiver)
    val extractedContactsToLibInstanceCollection = new ExtractedContactsToLibInstanceCollection("extractedContactsToLibInstanceCollection",stemLex,stemmer,notificationReceiver)
    extractedContactsToLibInstanceCollection.setInputData((exContacts,positive,negative))
    pipeline.add(extractedContactsToLibInstanceCollection)
    val note = pipeline.run()
    if (note.status == ProgressStatus.Completed) {
      allLibInstances = extractedContactsToLibInstanceCollection.get()
      logger.info(note.message)
      searchKeeperActor ! AlterSearchDetails(userId,searchId,25,SearchStatus.running,null)
      self ! CoClOpTrainModels
    }else {
      logger.error(note.message)
      searchKeeperActor ! AlterSearchDetails(userId,searchId,10,SearchStatus.failed,null)
      self ! PoisonPill
    }
  }

  private def train(): Unit = {
    val pipeline = new PipeLine("PipeLineProcedure", folder + "classification/")
    pipeline.setINoteReceiver(notificationReceiver)

    val createTrainingInstance = new LibInstanceCollectionToTrainingLibInstance("createTrainingInstance",notificationReceiver)
    createTrainingInstance.setInputData(allLibInstances)

    pipeline.add(createTrainingInstance).add(trainingComponent)
    val note = pipeline.run()
    if (note.status == ProgressStatus.Completed) {
      logger.info(note.message)
      searchKeeperActor ! AlterSearchDetails(userId,searchId,50,SearchStatus.running,null)
      self ! CoClOpPredict
    }else {
      logger.error(note.message)
      searchKeeperActor ! AlterSearchDetails(userId,searchId,25,SearchStatus.failed,null)
      self ! PoisonPill
    }
  }

  private def predict(): Unit = {
    val pipeline = new PipeLine("PipeLineProcedure", folder + "classification/")
    pipeline.setINoteReceiver(notificationReceiver)

    predictionComponent.setInputData((allLibInstances,trainingComponent.getOutputData()))


    pipeline.add(predictionComponent)
    val note = pipeline.run()
    if (note.status == ProgressStatus.Completed) {
      logger.info(note.message)
      searchKeeperActor ! AlterSearchDetails(userId,searchId,80,SearchStatus.running,null)
      self ! CoClOpAddToMongo
    }else {
      logger.error(note.message)
      searchKeeperActor ! AlterSearchDetails(userId,searchId,50,SearchStatus.failed,null)
      self ! PoisonPill
    }
  }

  private def addToMongo (): Unit = {
    val contactsIterator = exContacts.getContacts.iterator()
    val predictionsIterator = predictionComponent.getOutputData().iterator()

    val posInstances = new ListBuffer[ClassificationInfoPerInstance]
    val unlabeledInstances = new ListBuffer[ClassificationInfoPerInstance]
    val negInstances = new ListBuffer[ClassificationInfoPerInstance]

    while (contactsIterator.hasNext) {
      val contact = contactsIterator.next()
      val contactId = contact.getContactId
      val prediction = predictionsIterator.next()
      val prob = if (prediction.head._1 == 1) prediction.head._2 else prediction.tail.head._2
      if (positive.contains(contactId)) {
        posInstances += new ClassificationInfoPerInstance(contactId,prob,"desirable")
      }else if (negative.contains(contactId)) {
        negInstances += new ClassificationInfoPerInstance(contactId,prob,"undesirable")
      }else {
        unlabeledInstances += new ClassificationInfoPerInstance(contactId,prob,"unlabeled")
      }
    }

    val ret = new util.ArrayList[ClassificationInfoPerInstance]
    posInstances.sortWith((a,b) => a.getProbability > b.getProbability).foreach(e => ret.add(e))
    unlabeledInstances.sortWith((a,b) => a.getProbability > b.getProbability).foreach(e => ret.add(e))
    negInstances.sortWith((a,b) => a.getProbability > b.getProbability).foreach(e => ret.add(e))
    classifiedContacts.setSequence(ret)

    mongoTemplate.save(classifiedContacts)
    searchKeeperActor ! AlterSearchDetails(userId, searchId, 100, SearchStatus.completed, null)
    contactClassificationActor ! ContactExtractionOperationTerminatedMessage(userId + ":" + searchId)
    self ! PoisonPill
  }

  private def cancel (): Unit = {
    if (canceled == false) {
      canceled = true
      searchKeeperActor ! AlterSearchDetails(userId, searchId, 50, SearchStatus.canceled, null)
      contactClassificationActor ! ContactClassificationOperationTerminatedMessage(userId + ":" + searchId)
      self ! PoisonPill
    }
  }

  private def delete(file:File) {
    if (file.isDirectory) {
      file.listFiles().foreach(e => delete(e))
    }
    file.delete()
  }

  case object CoClOpCreateLibInstances
  case object CoClOpTrainModels
  case object CoClOpPredict
  case object CoClOpAddToMongo
}
