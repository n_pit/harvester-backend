package org.scify.contactharvesterbackend.inteligence.classification

import org.scify.contactharvesterbackend.bean.ExtractedContacts
import org.scify.pipeline.basic.{Note, ProgressStatus}
import org.scify.pipeline.interfaces._
import org.scify.pipeline.classificationcomponent.{LibInstance, LibInstanceCollection, StringLexicon}

import collection.mutable.HashSet
import collection.JavaConversions._
import scala.collection.mutable

/**
  * Created by akosmo on 3/6/2016.
  */
class ExtractedContactsToLibInstanceCollection(var name: String, stemLex: StringLexicon, stemmer: StemmersAndStopWordsParser, var noteReceiver: INoteReceiver) extends IPipeLineComponent[(ExtractedContacts, HashSet[Int], HashSet[Int]), LibInstanceCollection] {

  private var input: (ExtractedContacts, HashSet[Int], HashSet[Int]) = null
  private var output: LibInstanceCollection = null


  override def execute(): Note = {
    if (input == null)
      return new Note(getSender, "No input set", ProgressStatus.Error)
    else {
      try {
        output = new LibInstanceCollection
        val positive = input._2
        val negative = input._3
        val contacts = input._1
        contacts.getContacts.foreach(contact => {
          val text = contact.getAdditionalInfo
          val contactId = contact.getContactId
          val instance = new LibInstance
          val cat = if (positive.contains(contactId)) 1 else if (negative.contains(contactId)) -1 else 0
          instance.categories += cat
          val stemToFreq = new mutable.HashMap[Int, Int]
          text.split(" ").map(e => e.trim).foreach(word => {
            val w = stemmer.stem(word)
            if (w != "") {
              val id = stemLex.getId(w)
              stemToFreq.get(id) match {
                case Some(x) => stemToFreq += id -> (x + 1)
                case None => stemToFreq += id -> 1
              }
            }
          })
          val query = contact.getRelatedQueries
          query.split(" ").map(e => e.trim).foreach(word => {
            if (word != "contact" && word != "επικοινωνία" && word != "loc:gr") {
              val w = stemmer.stem(word)
              if (w != "") {
                val id = stemLex.getId(w)
                stemToFreq.get(id) match {
                  case Some(x) => stemToFreq += id -> (x + 1)
                  case None => stemToFreq += id -> 1
                }
              }
            }
          })
          stemToFreq.foreach(e => instance.addFeature(e._1, e._2))
          output.add(instance)
        })
        new Note(getSender, "Created", ProgressStatus.Completed)
      }catch {
        case e:Exception => new Note(getSender, "Failed with exception: (" + e + ")", ProgressStatus.Error)
      }
    }
  }

  override def getOutputData: LibInstanceCollection = output

  override def setInputData(input: (ExtractedContacts, HashSet[Int], HashSet[Int])): Unit = this.input = input

  override def get(): LibInstanceCollection = output

  override def load(fileName: String): Note = {
    output.loadFromSparseVectorFile(fileName)
    new Note(getSender, "Loaded successfully", ProgressStatus.Completed)
  }

  override def save(fileName: String): Note = {
    output.saveToSparseVectorFile(fileName)
    new Note(getSender, "Saved successfully", ProgressStatus.Completed)
  }

  override def setSender(name: String): Unit = this.name = name

  override def getSender: String = name

  override def setINoteReceiver(iNoteReceiver: INoteReceiver): Unit = noteReceiver = iNoteReceiver

  override def sendNote(note: Note): Unit = {
    noteReceiver.add(note)
  }
}
