package org.scify.contactharvesterbackend.inteligence

import java.io._
import java.net.{URL, URLConnection}
import java.util

import net.htmlparser.jericho.Source
import net.liftweb.json.JsonDSL._
import net.liftweb.json._
import org.scify.contactharvesterbackend.inteligence.contactExtraction._

import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

/**
  * Created by akosmo on 21/3/2016.
  */
object JsonTool {

  def extract(jsonString:String):util.ArrayList[String] = {
    val ret = new util.ArrayList[String]()
    val json = parse(jsonString)
    var links = compactRender(json \\ "Url").toString.substring(1)
    links = links.substring(0,links.length-1)
    val urlEntries = links.split("\",\"")
    urlEntries.foreach(e => {
      val link = e.substring(e.lastIndexOf('"') + 1)
      ret.add(link)
    })
    ret
  }

  def readContactsWithQueriesFromJson(fileName:String):List[ContactsWithQueries] = {
    val fr = new FileReader(fileName)
    val br = new BufferedReader(fr)
    val jsonStringBuffer = new StringBuffer()
    var line = br.readLine()
    while (line != null) {
      jsonStringBuffer.append(line)
      line = br.readLine()
    }
    br.close()
    fr.close()
    implicit val formats = DefaultFormats
    val json = JsonParser.parse(jsonStringBuffer.toString)
    json.extract[Map[String,List[ContactsWithQueries]]].head._2
  }

  def readVriskoJson(fileName:String):List[VriskoContact] = {
    val fr = new FileReader(fileName)
    val br = new BufferedReader(fr)
    val jsonStringBuffer = new StringBuffer()
    var line = br.readLine()
    while (line != null) {
      jsonStringBuffer.append(line)
      line = br.readLine()
    }
    br.close()
    fr.close()
    implicit val formats = DefaultFormats
    val json = JsonParser.parse(jsonStringBuffer.toString)
    json.extract[Map[String,List[VriskoContact]]].head._2
  }

  def addQueriesToJsonFile (fileName:String, linkToQueries:util.HashMap[String, util.HashSet[String]]): Unit ={
    val fr = new FileReader(fileName)
    val br = new BufferedReader(fr)
    val jsonStringBuffer = new StringBuffer()
    var line = br.readLine()
    while (line != null) {
      jsonStringBuffer.append(line)
      line = br.readLine()
    }
    br.close()
    fr.close()
    implicit val formats = DefaultFormats
    val json = JsonParser.parse(jsonStringBuffer.toString)
    val contactsPerLink = json.extract[Map[String,List[ContactsPerLinkExpanded]]].head._2

    var contactsPerLinkExpanded:List[ContactsPerLinkWithQueries] = Nil

    contactsPerLink.foreach((e:ContactsPerLinkExpanded) => {
      val name = e.url
      var queries:List[String] = Nil
      if (linkToQueries.containsKey(name)) {
        val iter = linkToQueries.get(name).iterator()
        while (iter.hasNext) {
          val query = iter.next()
          queries = query :: queries
        }
      }
      e.facebook.foreach(e1=>{
        println(e1.facebookUrl)
        e1.information.foreach(e2 => println(" " + e2._1 + " " + e2._2))
      })
      val expanded = ContactsPerLinkWithQueries(name,queries,e.facebook,e.twitter,e.linkedin)
      contactsPerLinkExpanded = expanded :: contactsPerLinkExpanded
    })

    val outPutjson =
      ("contactsPerLink" ->
        contactsPerLinkExpanded.map { e =>
          (("url" -> e.url) ~
            ("queries" -> e.queries) ~
            ("facebook" -> e.facebook.map { fe =>
              (("facebookUrl" -> fe.facebookUrl) ~
                ("information" -> fe.information))
            }) ~
            ("twitter" -> e.twitter.map { te =>
              (("twitterUrl" -> te.twitterUrl) ~
                ("location" -> te.location) ~
                ("url" -> te.url))

            }) ~
            ("linkedin" -> e.linkedin))
        })

    val outputFileName = fileName.substring(0,fileName.indexOf(".")) + "_withQueries.txt"

    val fw = new BufferedWriter(new FileWriter(new File(outputFileName)))
    fw.write(prettyRender(outPutjson))
    fw.close()
  }

  def retryFacebookFails (inputFileName:String,outputFileName:String): Unit ={
    val fr = new FileReader(inputFileName)
    val br = new BufferedReader(fr)
    val jsonStringBuffer = new StringBuffer()
    var line = br.readLine()
    while (line != null) {
      jsonStringBuffer.append(line)
      line = br.readLine()
    }
    br.close()
    fr.close()
    implicit val formats = DefaultFormats
    val json = JsonParser.parse(jsonStringBuffer.toString)
    val contactsPerLink:List[ContactsPerLinkWithQueries] = json.extract[Map[String,List[ContactsPerLinkWithQueries]]].head._2

    var contactsPerLinkExpanded:List[ContactsPerLinkWithQueries] = Nil

    var counter = contactsPerLink.size
    contactsPerLink.foreach(e => {
      val name = e.url
      val queries = e.queries

      var facebook:List[FacebookInfo] = Nil
      e.facebook.foreach(f => {
        val urlFacebook = f.facebookUrl
        val information = f.information
        if (information.size > 0) {
          facebook = f :: facebook
          println("Counter: " + counter + "(ok -- doing nothing)")
        }
        else {
          val info = new mutable.HashMap[String,String]()
          def operation(uRLConnection: URLConnection):Boolean = {
            val html = new Source(uRLConnection)

            val infoPart = html.getAllElementsByClass("fbSettingsList").iterator().next().getChildElements
            val iter = infoPart.iterator()
            while (iter.hasNext) {
              val element = iter.next()

              val partsIter = element.getAllElementsByClass("_50f4").iterator()

              val key = partsIter.next().getTextExtractor.toString
              val value = partsIter.next().getTextExtractor.toString

              info += key -> value
            }
            true
          }
          try {
            val url = new URL(urlFacebook)
            val con = url.openConnection()
            con.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0")
            implicit val ec = ExecutionContext.global
            val f = Future{operation(con)}
            Await.result(f,Duration.apply("10 seconds"))
            println("Counter: " + counter + " facebookUrl:" + urlFacebook + " gotNewContacts")
          }catch {
            case e:Exception => {
              println("Counter: " + counter + " facebookUrl:" + urlFacebook + " failed with exception + " + e)

            }
          }
          val fi = FacebookInfo(urlFacebook,info.toMap)
          facebook = fi :: facebook
        }
      })
      counter -= 1
      val expanded = ContactsPerLinkWithQueries(name,queries, facebook,e.twitter,e.linkedin)
      contactsPerLinkExpanded = expanded :: contactsPerLinkExpanded
    })

    val outPutjson =
      ("contactsPerLink" ->
        contactsPerLinkExpanded.map { e =>
          (("url" -> e.url) ~
            ("queries" -> e.queries) ~
            ("facebook" -> e.facebook.map { fe =>
              (("facebookUrl" -> fe.facebookUrl) ~
                ("information" -> fe.information))
            }) ~
            ("twitter" -> e.twitter.map { te =>
              (("twitterUrl" -> te.twitterUrl) ~
                ("location" -> te.location) ~
                ("url" -> te.url))

            }) ~
            ("linkedin" -> e.linkedin))
        })
    val fw = new BufferedWriter(new FileWriter(new File(outputFileName)))
    fw.write(prettyRender(outPutjson))
    fw.close()
  }

}
