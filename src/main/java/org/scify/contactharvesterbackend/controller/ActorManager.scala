package org.scify.contactharvesterbackend.controller

import akka.actor.{ActorRef, ActorSystem, Props}
import org.scify.contactharvesterbackend.basic.ContactHarvesterScheduler
import org.scify.contactharvesterbackend.inteligence.SearchKeeperActor

/**
  * Created by akosmo on 5/5/2016.
  */
class ActorManager {
  val actorSystem: ActorSystem = ActorSystem.apply("BasicControllerSystem")
  val searchkeeperActorProps: Props = Props.create(classOf[SearchKeeperActor])
  val searchkeeperActor: ActorRef = actorSystem.actorOf(searchkeeperActorProps)
  val contactHarvesterSchedulerProps: Props = Props.create(classOf[ContactHarvesterScheduler], searchkeeperActor, actorSystem)
  val basicActor: ActorRef = actorSystem.actorOf(contactHarvesterSchedulerProps)
}
