package org.scify.contactharvesterbackend.controller;

import akka.actor.ActorRef;
import org.apache.logging.log4j.LogManager;
import org.scify.contactharvesterbackend.basic.*;
import org.scify.contactharvesterbackend.bean.*;
import org.scify.contactharvesterbackend.enumerations.SearchStatus;
import org.scify.contactharvesterbackend.inteligence.classification.CancelContactsClassificationMessage;
import org.scify.contactharvesterbackend.inteligence.classification.StartContactClassificationMessage;
import org.scify.contactharvesterbackend.inteligence.contactExtraction.CancelContactsSearchMessage;
import org.scify.contactharvesterbackend.inteligence.contactExtraction.StartContactsSearchMessage;
import org.scify.contactharvesterbackend.inteligence.keywordsExtraction.CancelKeywordsSearchMessage;
import org.scify.contactharvesterbackend.inteligence.keywordsExtraction.StartKeywordsSearchMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import java.util.HashMap;
import java.util.List;

/**
 * Created by akosmo on 14/4/2016.
 */
@RestController
public class BasicController {
    private ActorManager am = new ActorManager();
    private boolean firstTime = true;


    @Autowired
    ServletContext servletContext;

    @Autowired
    private MongoTemplate mongoTemplate;

    @ModelAttribute("initialChecks")
    public void initialChecks() {
        if (firstTime) {
            Settings.readConfFile(servletContext);
            am.searchkeeperActor().tell(new SetMongoTemplate(mongoTemplate), ActorRef.noSender());
            am.basicActor().tell(new SetMongoTemplate(mongoTemplate), ActorRef.noSender());
            firstTime = false;
        }
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET, headers = "Accept=application/json")
    public String test() {
        try {
            return "WORKS";
        } catch (Exception e) {
            return "Exception: " + e.toString();
        }

    }

    @RequestMapping(value = "/searchStatus", method = RequestMethod.GET, headers = "Accept=application/json")
    public RestResponseMessage<SearchKeeper> allSearches(@RequestParam String key) {
        if (!key.equals(Settings.key())) {
            RestResponseMessage<SearchKeeper> ret = new RestResponseMessage<>("failed", "authentication failure", "searchStatus");
            return ret;
        }
        try {
            LogManager.getRootLogger().info("SearchStatus GET method was called");
            SearchKeeper sk = mongoTemplate.findById("SearchKeeperId", SearchKeeper.class, "searches");
            RestResponseMessage<SearchKeeper> ret = new RestResponseMessage<>("success", "", "searchStatus", sk);
            return ret;

        } catch (Exception e) {
            RestResponseMessage<SearchKeeper> ret = new RestResponseMessage<>("failed", e.toString(), "searchStatus");
            return ret;
        }
    }

    @RequestMapping(value = "/searchStatusOfUser", method = RequestMethod.GET, headers = "Accept=application/json")
    public RestResponseMessage<HashMap<Integer, SearchDetails>> searchesOfUser(@RequestParam String key, @RequestParam int userId) {
        if (!key.equals(Settings.key())) {
            RestResponseMessage<HashMap<Integer, SearchDetails>> ret = new RestResponseMessage<>("failed", "authentication failure", "searchStatus");
            return ret;
        }
        try {
            LogManager.getRootLogger().info("SearchStatusOfUser (" + userId + ") GET method was called");
            SearchKeeper sk = mongoTemplate.findById("SearchKeeperId", SearchKeeper.class, "searches");
            RestResponseMessage<HashMap<Integer, SearchDetails>> ret = new RestResponseMessage<>("success", "", "searchStatus", sk.getUserIdToSearches().get(userId));
            return ret;

        } catch (Exception e) {
            RestResponseMessage<HashMap<Integer, SearchDetails>> ret = new RestResponseMessage<>("failed", e.toString(), "searchStatus");
            return ret;
        }
    }
// ti url thelei gia na ftaseis edw
    // + to  rest key 
    @RequestMapping(value = "/extract_keywords", method = RequestMethod.POST, headers = "Accept=application/json")
    public RestResponseMessage<String> extractKeywordsStart(@RequestParam String key, @RequestParam int userId, @RequestParam int searchId, @RequestParam List<String> urls) {
        LogManager.getRootLogger().info("called extractKeywordsStart with userId:" + userId + ", searchId:" + searchId + " and urls:" + urls);
        if (!key.equals(Settings.key())) {
            RestResponseMessage<String> ret = new RestResponseMessage<>("failed", "authentication failure", "extract_keywords");
            return ret;
        }
        try {
            SearchKeeper sk = mongoTemplate.findById("SearchKeeperId", SearchKeeper.class, "searches");
            SearchDetails sd = sk.getSearchOfUser(userId, searchId);
            if (sd.getStatus() == SearchStatus.pending) {
                RestResponseMessage<String> ret = new RestResponseMessage<>("failed", "procedure is already pending, you must cancel it first", "extract_keywords");
                return ret;
            } else if (sd.getStatus() == SearchStatus.running) {
                RestResponseMessage<String> ret = new RestResponseMessage<>("failed", "procedure is already running, you must cancel it first", "extract_keywords");
                return ret;
            } else {
                am.basicActor().tell(new StartKeywordsSearchMessage(userId, searchId, urls), ActorRef.noSender());
                RestResponseMessage<String> ret = new RestResponseMessage<>("success", "procedure was initiated", "extract_keywords");
                return ret;
            }

        } catch (Exception e) {
            RestResponseMessage<String> ret = new RestResponseMessage<>("failed", e.toString(), "extract_keywords");
            return ret;
        }
    }

    @RequestMapping(value = "/extract_keywords", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public RestResponseMessage<String> extractKeywordsCancel(@RequestParam String key, @RequestParam int userId, @RequestParam int searchId) {
        if (!key.equals(Settings.key())) {
            RestResponseMessage<String> ret = new RestResponseMessage<>("failed", "authentication failure", "extract_keywords");
            return ret;
        }
        try {
            SearchKeeper sk = mongoTemplate.findById("SearchKeeperId", SearchKeeper.class, "searches");
            SearchDetails sd = sk.getSearchOfUser(userId, searchId);
            if (sd.getStatus() == SearchStatus.running || sd.getStatus() == SearchStatus.pending) {
                am.basicActor().tell(new CancelKeywordsSearchMessage(userId, searchId), ActorRef.noSender());
                RestResponseMessage<String> ret = new RestResponseMessage<>("success", "keyword extraction was canceled", "extract_keywords");
                return ret;
            } else {
                RestResponseMessage<String> ret = new RestResponseMessage<>("failed", "you cannot cancel a search that is not running or pending", "extract_keywords");
                return ret;
            }
        } catch (Exception e) {
            RestResponseMessage<String> ret = new RestResponseMessage<>("failed", e.toString(), "extract_keywords");
            return ret;
        }
    }

    @RequestMapping(value = "/extract_keywords", method = RequestMethod.GET, headers = "Accept=application/json")
    public RestResponseMessage<List<String>> extractKeywordsGet(@RequestParam String key, @RequestParam int userId, @RequestParam int searchId) {
        if (!key.equals(Settings.key())) {
            RestResponseMessage<List<String>> ret = new RestResponseMessage<>("failed", "authentication failure", "extract_keywords");
            return ret;
        }
        SearchKeeper sk = mongoTemplate.findById("SearchKeeperId", SearchKeeper.class, "searches");
        //SearchKeeper sk = mongoTeRestResponseMessagemplate.findById("SearchKeeperId", SearchKeeper.class, "searches");
        SearchDetails sd = sk.getSearchOfUser(userId, searchId);
        if (sd.getStatus() == SearchStatus.completed) {
            ExtractedKeywords ek = mongoTemplate.findById("ExtractedKeywords" + userId + ":" + searchId, ExtractedKeywords.class, "searches");
            RestResponseMessage<List<String>> ret = new RestResponseMessage<>("success", "", "extract_keywords", ek.getKeywords());
            return ret;
        } else {
            RestResponseMessage<List<String>> ret = new RestResponseMessage<>("failed", "the task is not completed yet", "extract_keywords", null);
            return ret;
        }
    }

    @RequestMapping(value = "/extract_contacts", method = RequestMethod.POST, headers = "Accept=application/json")
    public RestResponseMessage<String> extractContactsStart(@RequestParam String key, @RequestParam int userId, @RequestParam int searchId, @RequestParam List<String> keywords, @RequestParam int maxTopResultsPerSearch, @RequestParam String extraStringPerSearch) {
        if (!key.equals(Settings.key())) {
            RestResponseMessage<String> ret = new RestResponseMessage<>("failed", "authentication failure", "extract_contacts");
            return ret;
        }
        try {
            SearchKeeper sk = mongoTemplate.findById("SearchKeeperId", SearchKeeper.class, "searches");
            SearchDetails sd = sk.getSearchOfUser(userId, searchId);
            if (sd.getStatus() == SearchStatus.pending) {
                RestResponseMessage<String> ret = new RestResponseMessage<>("failed", "procedure is already pending, you must cancel it first", "extract_contacts");
                return ret;
            } else if (sd.getStatus() == SearchStatus.running) {
                RestResponseMessage<String> ret = new RestResponseMessage<>("failed", "procedure is already running, you must cancel it first", "extract_contacts");
                return ret;
            } else {
                am.basicActor().tell(new StartContactsSearchMessage(userId, searchId, keywords, maxTopResultsPerSearch, extraStringPerSearch), ActorRef.noSender());
                RestResponseMessage<String> ret = new RestResponseMessage<>("success", "procedure was initiated", "extract_contacts");
                return ret;
            }
        } catch (Exception e) {
            RestResponseMessage<String> ret = new RestResponseMessage<>("failed", e.toString(), "extract_contacts");
            return ret;
        }
    }

    @RequestMapping(value = "/extract_contacts", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public RestResponseMessage<String> extractContactsCancel(@RequestParam String key, @RequestParam int userId, @RequestParam int searchId) {
        if (!key.equals(Settings.key())) {
            RestResponseMessage<String> ret = new RestResponseMessage<>("failed", "authentication failure", "extract_contacts");
            return ret;
        }
        try {
            SearchKeeper sk = mongoTemplate.findById("SearchKeeperId", SearchKeeper.class, "searches");
            SearchDetails sd = sk.getSearchOfUser(userId, searchId);
            if (sd.getStatus() == SearchStatus.running || sd.getStatus() == SearchStatus.pending) {
                am.basicActor().tell(new CancelContactsSearchMessage(userId, searchId), ActorRef.noSender());
                RestResponseMessage<String> ret = new RestResponseMessage<>("success", "contacts extraction was canceled", "extract_contacts");
                return ret;
            } else {
                RestResponseMessage<String> ret = new RestResponseMessage<>("failed", "you cannot cancel a search that is not running or pending", "extract_contacts");
                return ret;
            }

        } catch (Exception e) {
            RestResponseMessage<String> ret = new RestResponseMessage<>("failed", e.toString(), "extract_contacts");
            return ret;
        }
    }

    @RequestMapping(value = "/extract_contacts", method = RequestMethod.GET, headers = "Accept=application/json")
    public RestResponseMessage<List<ExtractedContact>> extractContactsGet(@RequestParam int userId, @RequestParam int searchId, @RequestParam String key) {
        if (!key.equals(Settings.key())) {
            RestResponseMessage<List<ExtractedContact>> ret = new RestResponseMessage<>("failed", "authentication failure with", "extract_contacts");
            return ret;
        }
        try {
            SearchKeeper sk = mongoTemplate.findById("SearchKeeperId", SearchKeeper.class, "searches");
            SearchDetails sd = sk.getSearchOfUser(userId, searchId);
            if (sd.getStatus() == SearchStatus.completed) {
                ExtractedContacts ec = mongoTemplate.findById("ExtractedContacts" + userId + ":" + searchId, ExtractedContacts.class, "searches");
                RestResponseMessage<List<ExtractedContact>> ret = new RestResponseMessage<>("success", "", "extract_contacts", ec.getContacts());
                return ret;
            } else {
                RestResponseMessage<List<ExtractedContact>> ret = new RestResponseMessage<>("failed", "the search is not completed yet", "extract_contacts");
                return ret;
            }
        } catch (Exception e) {
            RestResponseMessage<List<ExtractedContact>> ret = new RestResponseMessage<>("failed", e.toString(), "extract_contacts");
            return ret;
        }
    }

    @RequestMapping(value = "/classify_contacts", method = RequestMethod.POST, headers = "Accept=application/json")
    public RestResponseMessage<String> classifyContactsStart(@RequestParam String key, @RequestParam int userId, @RequestParam int searchId, @RequestParam List<Integer> positive, @RequestParam List<Integer> negative) {
        if (!key.equals(Settings.key())) {
            RestResponseMessage<String> ret = new RestResponseMessage<>("failed", "authentication failure", "classify_contacts");
            return ret;
        }
        try {
            SearchKeeper sk = mongoTemplate.findById("SearchKeeperId", SearchKeeper.class, "searches");
            SearchDetails sd = sk.getSearchOfUser(userId, searchId);
            if (sd.getStatus() == SearchStatus.pending) {
                RestResponseMessage<String> ret = new RestResponseMessage<>("failed", "procedure is already pending, you must cancel it first", "classify_contacts");
                return ret;
            } else if (sd.getStatus() == SearchStatus.running) {
                RestResponseMessage<String> ret = new RestResponseMessage<>("failed", "procedure is already running, you must cancel it first", "classify_contacts");
                return ret;
            } else {
                am.basicActor().tell(new StartContactClassificationMessage(userId, searchId, positive, negative), ActorRef.noSender());
                RestResponseMessage<String> ret = new RestResponseMessage<>("success", "procedure was initiated", "classify_contacts");
                return ret;
            }
        } catch (Exception e) {
            RestResponseMessage<String> ret = new RestResponseMessage<>("failed", e.toString(), "classify_contacts");
            return ret;
        }
    }

    @RequestMapping(value = "/classify_contacts", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public RestResponseMessage<String> classifyContactsCancel(@RequestParam String key, @RequestParam int userId, @RequestParam int searchId) {
        if (!key.equals(Settings.key())) {
            RestResponseMessage<String> ret = new RestResponseMessage<>("failed", "authentication failure", "classify_contacts");
            return ret;
        }
        try {
            SearchKeeper sk = mongoTemplate.findById("SearchKeeperId", SearchKeeper.class, "searches");
            SearchDetails sd = sk.getSearchOfUser(userId, searchId);
            if (sd.getStatus() == SearchStatus.running || sd.getStatus() == SearchStatus.pending) {
                am.basicActor().tell(new CancelContactsClassificationMessage(userId, searchId), ActorRef.noSender());
                RestResponseMessage<String> ret = new RestResponseMessage<>("success", "contacts classification was canceled", "classify_contacts");
                return ret;
            } else {
                RestResponseMessage<String> ret = new RestResponseMessage<>("failed", "you cannot cancel a classification that is not running or pending", "classify_contacts");
                return ret;
            }

        } catch (Exception e) {
            RestResponseMessage<String> ret = new RestResponseMessage<>("failed", e.toString(), "classify_contacts");
            return ret;
        }
    }

    @RequestMapping(value = "/classify_contacts", method = RequestMethod.GET, headers = "Accept=application/json")
    public RestResponseMessage<List<ClassificationInfoPerInstance>> classifyContactsGet(@RequestParam int userId, @RequestParam int searchId, @RequestParam String key) {
        if (!key.equals(Settings.key())) {
            RestResponseMessage<List<ClassificationInfoPerInstance>> ret = new RestResponseMessage<>("failed", "authentication failure with", "classify_contacts");
            return ret;
        }
        try {
            SearchKeeper sk = mongoTemplate.findById("SearchKeeperId", SearchKeeper.class, "searches");
            SearchDetails sd = sk.getSearchOfUser(userId, searchId);
            if (sd.getStatus() == SearchStatus.completed) {
                ExtractedContactsClassificationSequence sequence = mongoTemplate.findById("ClassifiedContacts" + userId + ":" + searchId, ExtractedContactsClassificationSequence.class, "searches");
                RestResponseMessage<List<ClassificationInfoPerInstance>> ret = new RestResponseMessage<>("success", "", "classify_contacts", sequence.getSequence());
                return ret;
            } else {
                RestResponseMessage<List<ClassificationInfoPerInstance>> ret = new RestResponseMessage<>("failed", "the classification is not completed yet", "classify_contacts");
                return ret;
            }
        } catch (Exception e) {
            RestResponseMessage<List<ClassificationInfoPerInstance>> ret = new RestResponseMessage<>("failed", e.toString(), "classify_contacts");
            return ret;
        }
    }

}
