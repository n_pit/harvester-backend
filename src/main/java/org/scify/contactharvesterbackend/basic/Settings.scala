package org.scify.contactharvesterbackend.basic

import java.io.{BufferedReader, InputStream, InputStreamReader}
import javax.servlet.ServletContext

import org.apache.logging.log4j.LogManager

import scala.collection.mutable
import scala.io.Source
import scala.util.parsing.input.StreamReader

/**
  * Created by akosmo on 18/4/2016.
  */
object Settings {

  private val logger = LogManager.getRootLogger

  def readConfFile(servletContext:ServletContext): Unit = {
    val is: InputStream = servletContext.getResourceAsStream("/WEB-INF/resources/settings.conf")
    val fileLines = Source.fromInputStream(is).getLines()
    while (fileLines.hasNext) {
      val line = fileLines.next()
      if (line.trim != "" && line.charAt(0) != '#') {
        if (line.contains("=")) {
          val keyword = line.substring(0,line.indexOf('=')).trim
          val value = line.substring(line.indexOf('=')+1).trim
          keyword match {
            case "restKey" => {
              key = readFromFile(servletContext.getResourceAsStream("/WEB-INF/resources/sensitive/" + value))
            }
            case "bing_key" => {
              bingKey = readFromFile(servletContext.getResourceAsStream("/WEB-INF/resources/sensitive/" + value))
            }
            case "data" => {
              if (value.charAt(value.length-1) == '/')
                dataFolder = value
              else
                dataFolder = value + "/"
            }
            case "max_keyword_searches" => maxKeywordSearches = value.toInt
            case "max_contact_searches" => maxContactSearches = value.toInt
            case "max_contact_classifications" => maxContactClassifications = value.toInt
            case "min_keyword_length" => minKeywordLength = value.toInt
            case "max_keywords" => maxKeywordsCrawled = value.toInt
            case _ => logger.error("Wrong configuration tag: " + keyword);
          }
        }
      }
    }
    val isGreekStopWords = servletContext.getResourceAsStream("/WEB-INF/resources/stopwords_el.txt")
    Source.fromInputStream(isGreekStopWords).getLines().foreach(line => if (line != "") greekStopWords += line.trim)
    val isEnglishStopWords = servletContext.getResourceAsStream("/WEB-INF/resources/stopwords_en.txt")
    Source.fromInputStream(isEnglishStopWords).getLines().foreach(line => if (line != "") englishStopWords += line.trim)
  }

  private def readFromFile (input:InputStream):String = {
    try {
      val br = new BufferedReader(new InputStreamReader(input))
      val line = br.readLine()
      br.close()
      line
    }catch {
      case e:Exception => ""
    }
  }

  var dataFolder = ""
  var key = ""
  var bingKey = ""
  var maxKeywordSearches = 1
  var minKeywordLength = 3
  var maxKeywordsCrawled = 10
  var maxContactSearches = 1
  var maxContactClassifications = 1
  val greekStopWords = new mutable.HashSet[String]
  val englishStopWords = new mutable.HashSet[String]
}
