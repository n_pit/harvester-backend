package org.scify.contactharvesterbackend.basic

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import org.apache.logging.log4j.LogManager
import org.scify.contactharvesterbackend.bean.{ExtractedContacts, ExtractedContactsClassificationSequence, ExtractedKeywords, SearchKeeper}
import org.scify.contactharvesterbackend.enumerations.{SearchStatus, SearchStep}
import org.scify.contactharvesterbackend.inteligence._
import org.scify.contactharvesterbackend.inteligence.classification.{CancelContactsClassificationMessage, ContactClassifier, StartContactClassificationMessage}
import org.scify.contactharvesterbackend.inteligence.contactExtraction.{CancelContactsSearchMessage, ContactExtractor, StartContactsSearchMessage}
import org.scify.contactharvesterbackend.inteligence.keywordsExtraction.{CancelKeywordsSearchMessage, KeywordExtractor, StartKeywordsSearchMessage}
import org.springframework.data.mongodb.core.MongoTemplate

import collection.JavaConversions._

/**
  * Created by akosmo on 21/4/2016.
  */
class ContactHarvesterScheduler(searchKeeperActor: ActorRef, actorSystem: ActorSystem) extends Actor with ActorLogging {
  private var mongoTemplate: MongoTemplate = null
  private val logger = LogManager.getRootLogger
  private var keywordExtractor: ActorRef = null
  private var contactExtractor: ActorRef = null
  private var contactClassifier: ActorRef = null


  def receive = {
    case SetMongoTemplate(mongoTemplate) => {
      this.mongoTemplate = mongoTemplate
      initiateOtherActors()
    }
    case StartKeywordsSearchMessage(userId, searchId, urls) => startKeywordsSearch(userId, searchId, urls)
    case CancelKeywordsSearchMessage(userId,searchId) => cancelKeywordsSearch(userId,searchId)
    case StartContactsSearchMessage(userId, searchId, keywords,maxTopResultsPerSearch,extraStringPerSearch) => startContactsSearch(userId, searchId, keywords,maxTopResultsPerSearch,extraStringPerSearch)
    case CancelContactsSearchMessage(userId,searchId) => cancelContactsSearch(userId,searchId)
    case StartContactClassificationMessage(userId, searchId, positive,negative) => startContactsClassification(userId, searchId, positive,negative)
    case CancelContactsClassificationMessage(userId,searchId) => cancelContactsClassification(userId,searchId)

  }

  private def initiateOtherActors(): Unit = {
    val keywordExtractorProps: Props = Props(new KeywordExtractor(searchKeeperActor, actorSystem, mongoTemplate))
    keywordExtractor = actorSystem.actorOf(keywordExtractorProps)
    logger.info("Started: keywordExtractor actor")

    val contactExtractorProps: Props = Props(new ContactExtractor(searchKeeperActor, actorSystem, mongoTemplate))
    contactExtractor = actorSystem.actorOf(contactExtractorProps)
    logger.info("Started: contactExtractor actor")

    val contactClassificationProps: Props = Props(new ContactClassifier(searchKeeperActor, actorSystem, mongoTemplate))
    contactClassifier = actorSystem.actorOf(contactClassificationProps)
    logger.info("Started: contactClassifier actor")

    val sk: SearchKeeper = mongoTemplate.findById("SearchKeeperId", classOf[SearchKeeper],"searches")
    sk.getUserIdToSearches.foreach(e => {
      val userId = e._1
      e._2.foreach(search => {
        val searchId = search._1
        val details = search._2
        if (details.getStatus == SearchStatus.running || details.getStatus == SearchStatus.pending) {
          if (details.getStep == SearchStep.SearchingForKeywords) {
            val id = "ExtractedKeywords" + userId + ":" + searchId
            val exKeywords = mongoTemplate.findById(id,classOf[ExtractedKeywords])
            self ! StartKeywordsSearchMessage(userId,searchId,exKeywords.getUrls)
          }else if (details.getStep == SearchStep.SearchingForContacts) {
            val id = "ExtractedContacts" + userId + ":" + searchId
            val exContacts = mongoTemplate.findById(id,classOf[ExtractedContacts])
            self ! StartContactsSearchMessage(userId,searchId,exContacts.getKeywords,exContacts.getTopResultsPerSearch,exContacts.getExtraStringPerSearch)
          }else if (details.getStep == SearchStep.ClassifyingContacts) {
            val id = "ClassifiedContacts" + userId + ":" + searchId
            val classifiedContacts = mongoTemplate.findById(id,classOf[ExtractedContactsClassificationSequence])
            self ! StartContactClassificationMessage(userId,searchId,classifiedContacts.getPositive,classifiedContacts.getNegative)
          }
        }
      })
    })
    logger.info("Restarted running and pending actions")
  }

  private def startKeywordsSearch(userId: Int, searchId: Int, urls: java.util.List[String]): Unit = {
    logger.info("Entered: StartKeywordsSearch")

    searchKeeperActor ! StartKeywordsSearchMessage(userId, searchId)
    logger.info("Entered: StartKeywordsSearch -> message passed to searchKeeperActor")

    keywordExtractor ! StartKeywordsSearchMessage(userId, searchId, urls)
    logger.info("Entered: StartKeywordsSearch -> message passed to KeywordExtractor")
  }

  private def cancelKeywordsSearch(userId: Int, searchId: Int): Unit = {
    logger.info("Entered: CancelKeywordsSearch")

    searchKeeperActor ! AlterSearchDetails(userId,searchId,-1,SearchStatus.canceled,null)
    logger.info("Entered: CancelKeywordsSearch -> message passed to searchKeeperActor")

    keywordExtractor ! CancelKeywordsSearchMessage(userId,searchId)
    logger.info("Entered: CancelKeywordsSearch -> message passed to KeywordExtractor")
  }
// extraString : string that HAS to be in the keywords in each search (px contant) : restrictions  
  private def startContactsSearch(userId: Int, searchId: Int, keywords: java.util.List[String],maxTopResultsPerSearch:Int, extraStringPerSearch:String): Unit = {
    logger.info("Entered: StartContactsSearchMessage")

    searchKeeperActor ! StartContactsSearchMessage(userId, searchId)
    logger.info("Entered: StartContactSearch -> message passed to searchKeeperActor")

    contactExtractor ! StartContactsSearchMessage(userId,searchId,keywords,maxTopResultsPerSearch,extraStringPerSearch)
    logger.info("Entered: StartContactSearch -> message passed to ContactExtractor")
  }

  private def cancelContactsSearch(userId: Int, searchId: Int): Unit = {
    logger.info("Entered: CancelContactsSearch")

    searchKeeperActor ! AlterSearchDetails(userId,searchId,-1,SearchStatus.canceled,null)
    logger.info("Entered: CancelContactsSearch -> message passed to searchKeeperActor")

    contactExtractor ! CancelContactsSearchMessage(userId,searchId)
    logger.info("Entered: CancelContactsSearch -> message passed to ContactExtractor")
  }
// contact : ena row me contact info
  private def startContactsClassification(userId: Int, searchId: Int, positive: java.util.List[Integer],negative: java.util.List[Integer]): Unit = {
    logger.info("Entered: startContactsClassification")

    searchKeeperActor ! StartContactClassificationMessage(userId, searchId,positive,negative)
    logger.info("Entered: startContactsClassification -> message passed to searchKeeperActor")

    contactClassifier ! StartContactClassificationMessage(userId,searchId,positive,negative)
    logger.info("Entered: startContactsClassification -> message passed to ContactClassifier")
  }

  private def cancelContactsClassification(userId: Int, searchId: Int): Unit = {
    logger.info("Entered: cancelContactsClassification")

    searchKeeperActor ! AlterSearchDetails(userId,searchId,-1,SearchStatus.canceled,null)
    logger.info("Entered: cancelContactsClassification -> message passed to searchKeeperActor")

    contactClassifier ! CancelContactsClassificationMessage(userId,searchId)
    logger.info("Entered: cancelContactsClassification -> message passed to ContactClassifier")
  }

}

case object StartOperationMessage
case object CancelOperationMessage
case class SetMongoTemplate(mongoTemplate: MongoTemplate)
